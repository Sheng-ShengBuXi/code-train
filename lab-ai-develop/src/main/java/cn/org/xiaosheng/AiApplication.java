package cn.org.xiaosheng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AiApplication {
    public static void main(String[] args) {
        System.out.println();
        SpringApplication.run(AiApplication.class, args);
    }
}
