package cn.org.xiaosheng.exception;

import cn.org.xiaosheng.model.IError;

public class GlobalException extends RuntimeException {
    private IError i;

    public GlobalException(IError i) {
        this.i = i;
    }

    public IError getI() {
        return i;
    }
}
