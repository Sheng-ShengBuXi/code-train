package cn.org.xiaosheng.model;

public interface IError {

    int getCode();

    String getMessage();

}
