package cn.org.xiaosheng.redis;

public interface KeyPrefix {
    public int expireSeconds();

    public String getPrefix();

}
