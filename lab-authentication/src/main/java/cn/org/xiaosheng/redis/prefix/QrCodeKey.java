package cn.org.xiaosheng.redis.prefix;

import cn.org.xiaosheng.redis.BasePrefix;
import lombok.NonNull;

public class QrCodeKey extends BasePrefix {
    public QrCodeKey(@NonNull String prefix, int expireSeconds) {
        super(prefix, expireSeconds);
    }
    public QrCodeKey(String prefix){super(prefix);}

    public static QrCodeKey UUID = new QrCodeKey("uuid",300);
}