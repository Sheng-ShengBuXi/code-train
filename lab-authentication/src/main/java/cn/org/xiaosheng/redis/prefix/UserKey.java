package cn.org.xiaosheng.redis.prefix;

import cn.org.xiaosheng.redis.BasePrefix;

public class UserKey extends BasePrefix {

    public UserKey(String prefix, int expireSeconds) {
        super(prefix, expireSeconds);
    }

    public UserKey(String prefix) {
        super(prefix);
    }

    public static UserKey getById = new UserKey("id");

    public static UserKey getByName = new UserKey("name");
}
