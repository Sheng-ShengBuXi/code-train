package cn.org.xiaosheng.service;

import java.io.IOException;

public interface QRCodeService {

    String createQrCode(String content,int width,int height) throws IOException;

}
