package cn.org.xiaosheng.common.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

/**
 * @Description Redis切面类
 * @Author xiaosheng
 */
@Aspect
@Slf4j
public class RedisAspect {
    @Pointcut("execution(* cn.org.xiaosheng.common.utils.*(..))")
    public void pointcut(){
    }
    @Around("pointcut()")
    public Object handleException(ProceedingJoinPoint joinPoint){
        Object result = null;
        try {
            result= joinPoint.proceed();
        } catch (Throwable throwable) {
            log.error("Redis似乎出现了某些不可违因素");
        }
        return result;
    }

}
