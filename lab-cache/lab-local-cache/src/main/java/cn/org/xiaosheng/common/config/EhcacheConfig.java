package cn.org.xiaosheng.common.config;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

@Configuration
public class EhcacheConfig {

    // 创建缓存管理器
//    static CacheManager cacheManager = CacheManager.newInstance("../src/main/resources/ehcache.xml");
    static CacheManager cacheManager1;

    static {
        try {
            cacheManager1 = CacheManager.newInstance(new FileInputStream("D:\\Project\\JavaProject\\MyCode\\code-train\\lab-cache\\lab-local-cache\\src\\main\\resources\\ehcache.xml"));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Bean("userCache")
    public Cache cache() {
        // 从管理器获取缓存对象
        Cache cache = cacheManager1.getCache("userCache");
        /**
         * 这种方式也可以创建缓存对象，但是Ehcache官方并不推荐用该种方式进行Cache创建
         *          // 创建缓存配置
         *         Cache myCache = new Cache("myCache", 1000, false, false, 300, 200);
         *         // 将缓存添加到管理器
         *         cacheManager.addCache(myCache);
         */
        return cache;
    }

//    @Bean
//    public CacheManager cacheManager() {
//        return cacheManager;
//    }


}
