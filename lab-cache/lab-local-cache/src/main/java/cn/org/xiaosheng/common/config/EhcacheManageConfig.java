package cn.org.xiaosheng.common.config;

import net.sf.ehcache.CacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

/**
 * 配置Ehcache与SpringBoot集成
 */
@Configuration
public class EhcacheManageConfig {

    // 因为该注入会与CaffeineCache冲突，所以暂时注释，如果使用Ehcache需要注释CaffeineConfig的注入配置
//    @Bean
//    public CacheManager cacheManager() {
//        return CacheManager.create();
//    }
//
//    @Bean
//    public EhCacheManagerFactoryBean ehCacheManagerFactoryBean() {
//        EhCacheManagerFactoryBean cacheManagerFactoryBean = new EhCacheManagerFactoryBean();
//        cacheManagerFactoryBean.setConfigLocation(new ClassPathResource("ehcache.xml"));
//        cacheManagerFactoryBean.setShared(true);
//        return cacheManagerFactoryBean;
//    }

}
