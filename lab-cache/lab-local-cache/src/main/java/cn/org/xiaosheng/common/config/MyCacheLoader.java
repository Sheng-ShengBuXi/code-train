package cn.org.xiaosheng.common.config;

import net.sf.ehcache.CacheException;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Status;
import net.sf.ehcache.loader.CacheLoader;

import java.util.Collection;
import java.util.Map;

public class MyCacheLoader implements CacheLoader {

    @Override
    public Object load(Object key) throws CacheException {
        // 从数据库或其他数据源加载数据
        return null;
    }

    @Override
    public Map loadAll(Collection keys) {
        return Map.of();
    }

    @Override
    public Object load(Object key, Object argument) {
        return null;
    }

    @Override
    public Map loadAll(Collection keys, Object argument) {
        return Map.of();
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public CacheLoader clone(Ehcache cache) throws CloneNotSupportedException {
        return null;
    }

    @Override
    public void init() {

    }

    @Override
    public void dispose() throws CacheException {

    }

    @Override
    public Status getStatus() {
        return null;
    }
}
