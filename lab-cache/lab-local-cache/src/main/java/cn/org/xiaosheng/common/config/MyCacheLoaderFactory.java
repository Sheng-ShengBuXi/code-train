package cn.org.xiaosheng.common.config;

import net.sf.ehcache.Ehcache;
import net.sf.ehcache.config.Configuration;
import net.sf.ehcache.loader.CacheLoader;
import net.sf.ehcache.loader.CacheLoaderFactory;

import java.util.Properties;

public class MyCacheLoaderFactory extends CacheLoaderFactory {

//    public CacheLoader createCacheLoader(Ehcache cache, Configuration configuration) {
//        return new MyCacheLoader();
//    }

    @Override
    public CacheLoader createCacheLoader(Ehcache cache, Properties properties) {
        return new MyCacheLoader();
    }

}
