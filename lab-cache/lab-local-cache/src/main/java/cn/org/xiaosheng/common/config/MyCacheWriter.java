package cn.org.xiaosheng.common.config;

import net.sf.ehcache.CacheEntry;
import net.sf.ehcache.CacheException;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.writer.CacheWriter;
import net.sf.ehcache.writer.writebehind.operations.SingleOperationType;

import java.util.Collection;

public class MyCacheWriter implements CacheWriter {

    @Override
    public CacheWriter clone(Ehcache cache) throws CloneNotSupportedException {
        return null;
    }

    @Override
    public void write(Element element) throws CacheException {
        // 将数据写入数据库或其他数据源
    }

    @Override
    public void writeAll(Collection<Element> elements) throws CacheException {

    }

    @Override
    public void dispose() throws CacheException {

    }

    @Override
    public void init() {

    }

    @Override
    public void delete(CacheEntry entry) throws CacheException {

    }

    @Override
    public void deleteAll(Collection<CacheEntry> entries) throws CacheException {

    }

    @Override
    public void throwAway(Element element, SingleOperationType operationType, RuntimeException e) {

    }
}
