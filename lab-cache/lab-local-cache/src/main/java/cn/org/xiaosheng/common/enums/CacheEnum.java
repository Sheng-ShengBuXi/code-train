package cn.org.xiaosheng.common.enums;


public enum CacheEnum {

    CAFFEINE_100("caffeine100", 1000L), CAFFEINE_10("caffeine10", 10L);

    private String name;

    private Long expires;


    public String getName() {
        return name;
    }

    public Long getExpires() {
        return expires;
    }

    CacheEnum(String name, Long expires) {
        this.name = name;
        this.expires = expires;
    }


}
