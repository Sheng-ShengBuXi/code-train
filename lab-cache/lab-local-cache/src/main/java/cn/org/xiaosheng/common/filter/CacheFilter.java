package cn.org.xiaosheng.common.filter;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CacheFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        // 设置缓存策略,缓存一小时
//        httpResponse.setHeader("Cache-Control", "max-age=3600");
//        // 设置一个绝对的过期时间，超过这个时间点后浏览器将不再使用缓存的内容而向服务器请求新的资源, 这里设置过期时间一小时
//        httpResponse.setDateHeader("Expires", System.currentTimeMillis() + 3600 * 1000);
//        System.out.println("拦截器进行了拦截!");
        // 使缓存通行
        chain.doFilter(request, response);
    }
}
