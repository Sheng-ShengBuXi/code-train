package cn.org.xiaosheng.common.listener;

import net.sf.ehcache.CacheException;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.event.CacheEventListener;

public class MyCacheEventListener implements CacheEventListener {

    @Override
    public void notifyElementRemoved(Ehcache cache, Element element) throws CacheException {
        System.out.println("元素被移除: " + element.getObjectKey());
    }

    @Override
    public void notifyElementPut(Ehcache cache, Element element) throws CacheException {

    }

    @Override
    public void notifyElementUpdated(Ehcache cache, Element element) throws CacheException {

    }

    @Override
    public void notifyElementExpired(Ehcache cache, Element element) {

    }

    @Override
    public void notifyElementEvicted(Ehcache cache, Element element) {
        System.out.println("元素被逐出: " + element.getObjectKey());
    }

    @Override
    public void notifyRemoveAll(Ehcache cache) {

    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        // clone方法应该总是抛出CloneNotSupportedException，因为Ehcache要求CacheEventListener不可克隆。
        throw new CloneNotSupportedException();
    }

    @Override
    public void dispose() {

    }
}
