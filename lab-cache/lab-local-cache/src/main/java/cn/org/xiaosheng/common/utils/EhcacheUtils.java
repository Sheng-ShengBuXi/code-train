package cn.org.xiaosheng.common.utils;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * EhCache 缓存工具类
 * 使用String格式的value
 */
//@Component
public class EhcacheUtils {

//    @Autowired
    private CacheManager cacheManager;

    // 默认的缓存存在时间（秒）
    private static final int DEFAULT_LIVE_SECOND = 20 * 60;

    public static final String EHCACHE_KEY = "ehcache";

    /**
     * 添加缓存
     *
     * @param cacheName         xml中缓存名字
     * @param key
     * @param value
     * @param timeToLiveSeconds 缓存生存时间（秒）
     */
    public void set(String cacheName, String key, Object value, int timeToLiveSeconds) {
        Cache cache = cacheManager.getCache(cacheName);
        Element element = new Element(
                key, value,
                0,// timeToIdleSeconds=0
                timeToLiveSeconds);
        cache.put(element);
    }

    /**
     * 添加缓存
     * 使用默认生存时间
     *
     * @param cacheName xml中缓存名字
     * @param key
     * @param value
     */
    public void set(String cacheName, String key, Object value) {
        Cache cache = cacheManager.getCache(cacheName);
        Element element = new Element(
                key, value,
                0,// timeToIdleSeconds
                DEFAULT_LIVE_SECOND);
        cache.put(element);
    }

    /**
     * 添加缓存
     *
     * @param cacheName         xml中缓存名字
     * @param key
     * @param value
     * @param timeToIdleSeconds 对象空闲时间，指对象在多长时间没有被访问就会失效。
     *                          只对eternal为false的有效。传入0，表示一直可以访问。以秒为单位。
     * @param timeToLiveSeconds 缓存生存时间（秒）
     *                          只对eternal为false的有效
     */
    public void set(String cacheName, String key, Object value, int timeToIdleSeconds, int timeToLiveSeconds) {
        Cache cache = cacheManager.getCache(cacheName);
        Element element = new Element(
                key, value,
                timeToIdleSeconds,
                timeToLiveSeconds);
        cache.put(element);
    }

    /**
     * 添加缓存
     *
     * @param cacheName xml中缓存名字
     * @param key
     * @return
     */
    public Object get(String cacheName, String key) {
        Cache cache = cacheManager.getCache(cacheName);
        Element element = cache.get(key);
        if (element == null) {
            return null;
        }
        return element.getObjectValue();
    }

    /**
     * 删除缓存数据
     *
     * @param cacheName
     * @param key
     */
    public void delete(String cacheName, String key) {
        try {
            Cache cache = cacheManager.getCache(cacheName);
            cache.remove(key);
        } catch (Exception e) {

        }
    }
}

