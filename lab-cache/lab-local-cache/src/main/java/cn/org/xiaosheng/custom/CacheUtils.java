package cn.org.xiaosheng.custom;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 采用定时清楚缓存的方式
 * 新增容器MyCache，在容器中装载key(键), value(值), expiretime(过期时间)
 */
public class CacheUtils {

    /**
     * 缓存数据Map
     */
    private static final Map<String, MyCache> CACHE_MAP = new ConcurrentHashMap<String, MyCache>();

    /**
     * 定时器线程池，用于清除过期缓存
     */
    private static final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();


    static {
        // 注册一个定时线程任务，服务启动1秒之后，每隔500毫秒执行一次
        // 定时清理过期缓存
        executorService.scheduleAtFixedRate(CacheUtils::clearCache, 1000, 500, TimeUnit.MILLISECONDS);
    }

    /**
     * 添加缓存
     *
     * @param key    缓存键
     * @param value  缓存值
     * @param expire 过期时间，单位秒
     */
    public static void put(String key, Object value, long expire) {
        MyCache myCache = new MyCache();
        myCache.setKey(key);
        myCache.setValue(value);
        if (expire > 0) {
            /**
             * Duration.ofSeconds(expire).toMillis() 是Java 8中 java.time.Duration 类的一个使用方式。
             * 这段代码的含义是将一段时长（用秒数表示）转化为毫秒数。具体来说，Duration.ofSeconds(expire) 会创建一个表示 expire 秒的时长，然后 toMillis() 方法会将这个时长转换为毫秒数。
             * 例如，如果 expire 的值为60（即60秒或1分钟），那么 Duration.ofSeconds(expire).toMillis() 将返回60000，表示60000毫秒或1分钟。
             */
            long expireTime = System.currentTimeMillis() + Duration.ofSeconds(expire).toMillis();
            myCache.setExpireTime(expireTime);
        }
        CACHE_MAP.put(key, myCache);
    }

    /**
     * 获取缓存
     *
     * @param key 缓存键
     * @return 缓存数据
     */
    public static Object get(String key) {
        if (CACHE_MAP.containsKey(key)) {
            return CACHE_MAP.get(key).getValue();
        }
        return null;
    }

    /**
     * 移除缓存
     *
     * @param key 缓存键
     */
    public static void remove(String key) {
        CACHE_MAP.remove(key);
    }

    /**
     * 清理过期的缓存数据
     */
    private static void clearCache() {
        if (CACHE_MAP.size() <= 0) {
            return;
        }
        // 判断是否过期 过期就从缓存Map删除这个元素
        CACHE_MAP.entrySet().removeIf(entry -> entry.getValue().getExpireTime() != null && entry.getValue().getExpireTime() > System.currentTimeMillis());
    }

}
