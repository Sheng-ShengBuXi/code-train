package cn.org.xiaosheng.custom;

import lombok.Data;

@Data
public class MyCache {

    /**
     * 键
     */
    private String key;

    /**
     * 值
     */
    private Object value;

    /**
     * 过期时间
     */
    private Long expireTime;


}
