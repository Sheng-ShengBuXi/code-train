package cn.org.xiaosheng.listener;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class CacheWarmer implements ApplicationListener<ContextRefreshedEvent> {

    public static Map<String, String> cacheMap = new HashMap<String, String>();

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        cacheMap.put("xiaosheng", "xiaosheng");
    }
}
