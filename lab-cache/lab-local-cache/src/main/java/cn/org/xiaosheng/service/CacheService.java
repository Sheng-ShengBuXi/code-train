package cn.org.xiaosheng.service;

public interface CacheService {

    String getCacheById(String id);

    void deleteCacheById(String id);

    String updateCacheById(String id);

    String findCacheById(String id);

    String getDataFromDatabase(String key);

}
