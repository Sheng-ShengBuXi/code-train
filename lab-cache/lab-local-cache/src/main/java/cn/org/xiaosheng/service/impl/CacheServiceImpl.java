package cn.org.xiaosheng.service.impl;

import cn.org.xiaosheng.service.CacheService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class CacheServiceImpl implements CacheService {

    @Override
    @Cacheable(value = "caffeine100",key = "#id")  // 可以保存该方法的返回值
    public String getCacheById(String id) {
        System.out.println("查询了数据库");
        return id;
    }

    @Override
    @CacheEvict(value = "caffeine100",key = "#id")
    public void deleteCacheById(String id) {

    }

    @Override
    @CachePut(value = "caffeine100",key = "#id") // 可以保存(修改)该方法的返回值
    public String updateCacheById(String id) {
        return id + "###";
    }

    @Override
    @Cacheable(value = "caffeine_10",key = "#id") // 可以保存(修改)该方法的返回值
    public String findCacheById(String id) {
        return "我的命名空间是cache_10" + id;
    }

    public String getDataFromDatabase(String key) {
        // 这里是模拟从数据库中加载数据
        return "Data for " + key;
    }

}
