package cn.org.xiaosheng.test;

import cn.org.xiaosheng.custom.CacheUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.TimeUnit;

@SpringBootTest
public class LRUCacheTest {

    @Test
    void contextLoads() throws InterruptedException {
        // 写入缓存数据 2秒后过期
        CacheUtils.put("name", "qx", 2);

        Object value1 = CacheUtils.get("name");
        System.out.println("第一次查询结果：" + value1);

        // 停顿3秒
        TimeUnit.SECONDS.sleep(3);

        Object value2 = CacheUtils.get("name");
        System.out.println("第二次查询结果：" + value2);
    }

}
