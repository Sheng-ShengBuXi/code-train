package cn.org.xiaosheng.test;

import cn.org.xiaosheng.common.utils.RedisUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;

@SpringBootTest
public class RedisCacheTest {

    @Autowired
    private RedisTemplate<String, String> template;

    @Resource
    private RedisUtils redisUtils;

    @Test
    public void test() {
        // 初始化需要设置一些值和比如序列化方式等等,否则会报错
//        RedisTemplate<String, String> template = new RedisTemplate<>();
        template.opsForValue().set("name","Alice");
        String name = template.opsForValue().get("name");
        System.out.println("获取到的值：" + name); // 输出：获取到的值：Alice
        template.delete("name");
    }

    @Test
    public void saveValue() {
        //存入Redis
        redisUtils.set("username", "Hello XiaoSheng!");
        System.out.println("保存成功！！！");
        //根据key取出
        String username = (String) redisUtils.get("username");
        System.out.println("username="+username);
        redisUtils.del("username");
    }
}
