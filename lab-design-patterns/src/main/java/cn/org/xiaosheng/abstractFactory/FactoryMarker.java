package cn.org.xiaosheng.abstractFactory;

import cn.org.xiaosheng.abstractFactory.factory.ElfKingdomFactory;
import cn.org.xiaosheng.abstractFactory.factory.OrcKingdomFactory;
import cn.org.xiaosheng.abstractFactory.factory.interfaces.KingdomFactory;

/**
 * @author XiaoSheng
 * @date 2024/7/29 下午3:02
 */
public class FactoryMarker {

    public enum KingdomType {
        ELF, ORC
    }

    public KingdomFactory makeFactory(KingdomType type) {
         switch (type) {
            case ELF:
                return new ElfKingdomFactory();
            case ORC:
                return new OrcKingdomFactory();
        }
        return null;
    }

}
