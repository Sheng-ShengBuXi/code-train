package cn.org.xiaosheng.abstractFactory;

import cn.org.xiaosheng.abstractFactory.factory.interfaces.KingdomFactory;

/**
 * @author XiaoSheng
 * @date 2024/7/29 下午3:04
 */
public class Main {

    public static void main(String[] args) {
        FactoryMarker factoryMarker = new FactoryMarker();
        KingdomFactory kingdomFactory = factoryMarker.makeFactory(FactoryMarker.KingdomType.ORC);
        KingdomFactory kingdomFactory1 = factoryMarker.makeFactory(FactoryMarker.KingdomType.valueOf("ELF"));
        System.out.println(FactoryMarker.KingdomType.values()[1]);
        System.out.println(kingdomFactory.createArmy().getDescription());
        System.out.println(kingdomFactory.createCastle().getDescription());
        System.out.println(kingdomFactory.createKing().getDescription());

        System.out.println("ORC系列制作完毕!");
        System.out.println(kingdomFactory1.createKing().getDescription());
        System.out.println(kingdomFactory1.createCastle().getDescription());
        System.out.println(kingdomFactory1.createArmy().getDescription());
        System.out.println("结束");

    }

}
