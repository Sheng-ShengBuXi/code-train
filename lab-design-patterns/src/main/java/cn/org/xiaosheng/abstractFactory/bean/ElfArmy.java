package cn.org.xiaosheng.abstractFactory.bean;

import cn.org.xiaosheng.abstractFactory.bean.interfaces.Army;

/**
 * @author XiaoSheng
 * @date 2024/7/29 下午2:36
 */
public class ElfArmy implements Army {

    static final String DESCRIPTION = "This is the elven Army!";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
