package cn.org.xiaosheng.abstractFactory.bean;

import cn.org.xiaosheng.abstractFactory.bean.interfaces.King;

/**
 * @author XiaoSheng
 * @date 2024/7/29 下午2:37
 */
public class ElfKing implements King {

    static final String DESCRIPTION = "This is the elven Army!";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
