package cn.org.xiaosheng.abstractFactory.bean;

import cn.org.xiaosheng.abstractFactory.bean.interfaces.Castle;

/**
 * @author XiaoSheng
 * @date 2024/7/29 下午2:51
 */
public class OrcCastle implements Castle {
    static final String DESCRIPTION = "This is the Orc Castle!";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
