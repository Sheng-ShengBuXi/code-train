package cn.org.xiaosheng.abstractFactory.bean.interfaces;

/**
 * @author XiaoSheng
 * @date 2024/7/29 下午2:34
 */
public interface Castle {
    String getDescription();
}
