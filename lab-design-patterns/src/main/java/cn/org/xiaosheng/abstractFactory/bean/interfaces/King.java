package cn.org.xiaosheng.abstractFactory.bean.interfaces;

/**
 * @author XiaoSheng
 * @date 2024/7/29 下午2:35
 */
public interface King {
    String getDescription();
}
