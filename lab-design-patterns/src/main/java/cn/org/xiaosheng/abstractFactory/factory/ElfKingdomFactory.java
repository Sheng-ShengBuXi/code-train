package cn.org.xiaosheng.abstractFactory.factory;

import cn.org.xiaosheng.abstractFactory.bean.ElfArmy;
import cn.org.xiaosheng.abstractFactory.bean.ElfCastle;
import cn.org.xiaosheng.abstractFactory.bean.ElfKing;
import cn.org.xiaosheng.abstractFactory.factory.interfaces.KingdomFactory;
import cn.org.xiaosheng.abstractFactory.bean.interfaces.Army;
import cn.org.xiaosheng.abstractFactory.bean.interfaces.Castle;
import cn.org.xiaosheng.abstractFactory.bean.interfaces.King;

/**
 * @author XiaoSheng
 * @date 2024/7/29 下午2:59
 */
public class ElfKingdomFactory implements KingdomFactory {

    @Override
    public Castle createCastle() {
        return new ElfCastle();
    }

    @Override
    public King createKing() {
        return new ElfKing();
    }

    @Override
    public Army createArmy() {
        return new ElfArmy();
    }
}
