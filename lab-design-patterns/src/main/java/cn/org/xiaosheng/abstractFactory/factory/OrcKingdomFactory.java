package cn.org.xiaosheng.abstractFactory.factory;

import cn.org.xiaosheng.abstractFactory.bean.OrcArmy;
import cn.org.xiaosheng.abstractFactory.bean.OrcCastle;
import cn.org.xiaosheng.abstractFactory.bean.OrcKing;
import cn.org.xiaosheng.abstractFactory.factory.interfaces.KingdomFactory;
import cn.org.xiaosheng.abstractFactory.bean.interfaces.Army;
import cn.org.xiaosheng.abstractFactory.bean.interfaces.Castle;
import cn.org.xiaosheng.abstractFactory.bean.interfaces.King;

/**
 * @author XiaoSheng
 * @date 2024/7/29 下午3:00
 */
public class OrcKingdomFactory implements KingdomFactory {

    @Override
    public Castle createCastle() {
        return new OrcCastle();
    }

    @Override
    public King createKing() {
        return new OrcKing();
    }

    @Override
    public Army createArmy() {
        return new OrcArmy();
    }
}
