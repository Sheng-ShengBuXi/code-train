package cn.org.xiaosheng.abstractFactory.factory.interfaces;

import cn.org.xiaosheng.abstractFactory.bean.interfaces.Army;
import cn.org.xiaosheng.abstractFactory.bean.interfaces.Castle;
import cn.org.xiaosheng.abstractFactory.bean.interfaces.King;

/**
 * @author XiaoSheng
 * @date 2024/7/29 下午2:58
 */
public interface KingdomFactory {

    Castle createCastle();

    King createKing();

    Army createArmy();

}
