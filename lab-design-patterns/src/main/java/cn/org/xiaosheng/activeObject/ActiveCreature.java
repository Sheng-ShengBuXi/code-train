package cn.org.xiaosheng.activeObject;

import java.util.concurrent.*;

/**
 * @author XiaoSheng
 * @date 2024/7/29 下午4:03
 */

public abstract class ActiveCreature {

    ThreadLocal<String> threadLocal = new ThreadLocal<>();

    protected BlockingQueue<Runnable> requests;

    private String name;

    private Thread thread;

    private ThreadPoolExecutor threadPool = new ThreadPoolExecutor(
            5, // 核心线程数
                    10, // 最大线程数
                    1, // 空闲线程存活时间为1秒
            TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(100), // 任务队列大小为100
            Executors.defaultThreadFactory(), // 使用默认线程工厂
                    new ThreadPoolExecutor.AbortPolicy() // 拒绝策略为中止策略
            );


    // 采用线程池进行调度, 减少了线程多开的不足
    public ActiveCreature(String name) {
        this.name = name;
        this.requests = new LinkedBlockingQueue<Runnable>();
//        this.thread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                while (true) {
//                    try {
//                        threadLocal.set("xiaosheng");
//                        System.out.println("线程" + name + "开始执行了");
//
//                        requests.take().run();
//                    } catch (InterruptedException e) {
//                        System.out.println("出现队列任务执行异常错误!");
//                    }
//                }
//            }
//        });
//        thread.start();

    }

    public void run() {
        try {
            threadPool.submit(requests.take());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }




 //  通过阻塞队列进行实现, 缺点是会出现多个Thread
//    public ActiveCreature(String name) {
//        this.name = name;
//        this.requests = new LinkedBlockingQueue<Runnable>();
//         this.thread = new Thread(new Runnable() {
//             @Override
//             public void run() {
//                while (true) {
//                    try {
//                        threadLocal.set("xiaosheng");
//                        System.out.println("线程" + name + "开始执行了");
//
//                        requests.take().run();
//                    } catch (InterruptedException e) {
//                        System.out.println("出现队列任务执行异常错误!");
//                    }
//                }
//             }
//         });
//         thread.start();
//    }

//    public ActiveCreature(String name) {
//        this.name = name;
//        this.requests = new LinkedBlockingQueue<Runnable>();
//        this.thread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                while (true) {
//                    try {
//                        threadLocal.set("xiaosheng");
//                        System.out.println("线程" + name + "开始执行了");
//
//                        requests.take().run();
//                    } catch (InterruptedException e) {
//                        System.out.println("出现队列任务执行异常错误!");
//                    }
//                }
//            }
//        });
//        thread.start();
//    }

    public void eat() throws InterruptedException {
//        requests.put(new Runnable() {
//                         @Override
//                         public void run() {
//                             System.out.print( name() +" is eating!");
//                             System.out.println(name() + " has finished eating!");
//                         }
//                     }
//        );
    }

    public void roam() throws InterruptedException {
        requests.put(new Runnable() {
                         @Override
                         public void run() {
                             System.out.println(name() + " has started to roam the wastelands.");
                         }
                     }
        );
        run();
    }

    public String name() {
        return this.name;
    }

    protected void add(Runnable runnable) throws InterruptedException {
        requests.put(runnable);
        run();
    }


    // 采用线程池的方式进行实现, 通过开放扩展接口的形式供子类添加任务
//    protected <E> Future<E> add(Runnable runnable) {
//        return (Future<E>) threadPool.submit(runnable);
//    }

}
