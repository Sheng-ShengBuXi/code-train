package cn.org.xiaosheng.activeObject;



import java.util.ArrayList;
import java.util.List;

/**
 * @author XiaoSheng
 * @date 2024/7/29 下午4:22
 */
public class App implements Runnable{

    private static final int NUM_CREATURES = 3;

    public static void main(String[] args) {
       App app = new App();
        app.run();
    }

    @Override
    public void run() {
        List<ActiveCreature> creatures = new ArrayList<ActiveCreature>();
        try  {
            for (int i = 0; i < NUM_CREATURES; i++ ) {
                creatures.add(new Orc(Orc.class.getSimpleName() + i));
                creatures.get(i).eat();
                System.out.println("添加eat任务方法");
                creatures.get(i).roam();
                System.out.println("添加roam任务方法");
            }
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
            Thread.currentThread().interrupt();
        }
    }
}



