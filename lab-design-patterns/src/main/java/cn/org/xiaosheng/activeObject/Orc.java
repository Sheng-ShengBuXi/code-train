package cn.org.xiaosheng.activeObject;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @author XiaoSheng
 * @date 2024/7/29 下午4:21
 */
public class Orc extends ActiveCreature{

    public Orc (String name) {
        super(name);
    }

    @Override
    public void eat() throws InterruptedException {
        requests.put(new Runnable() {
            @Override
            public void run() {
                System.out.println("你好，这里是西西弗餐厅，欢迎来到食堂用餐");
            }
        });
    }
}
