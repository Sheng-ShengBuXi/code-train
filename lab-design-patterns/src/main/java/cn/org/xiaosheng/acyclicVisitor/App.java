package cn.org.xiaosheng.acyclicVisitor;

import cn.org.xiaosheng.acyclicVisitor.bean.Hayes;
import cn.org.xiaosheng.acyclicVisitor.bean.Zoom;
import cn.org.xiaosheng.acyclicVisitor.visitor.ConfigureForDosVisitor;
import cn.org.xiaosheng.acyclicVisitor.visitor.ConfigureForUnixVisitor;

/**
 * @author XiaoSheng
 * @date 2024/7/31 下午3:58
 */
public class App {

    public static void main(String[] args) {
        ConfigureForDosVisitor dosVisitor = new ConfigureForDosVisitor();
        ConfigureForUnixVisitor unixVisitor = new ConfigureForUnixVisitor();

        Zoom zoom = new Zoom();
        Hayes hayes = new Hayes();

        hayes.accept(dosVisitor);
        hayes.accept(unixVisitor);
        zoom.accept(dosVisitor);
        zoom.accept(unixVisitor);

    }


}
