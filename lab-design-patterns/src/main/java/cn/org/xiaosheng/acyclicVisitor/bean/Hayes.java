package cn.org.xiaosheng.acyclicVisitor.bean;

import cn.org.xiaosheng.acyclicVisitor.bean.interfaces.Modem;
import cn.org.xiaosheng.acyclicVisitor.visitor.interfaces.HayesVisitor;
import cn.org.xiaosheng.acyclicVisitor.visitor.interfaces.ModemVisitor;

/**
 * @author XiaoSheng
 * @date 2024/7/31 下午3:46
 */
public class Hayes implements Modem {

    @Override
    public void accept(ModemVisitor modemVisitor) {
        // instanceof 是 Java 中的一个关键字，用于判断一个对象是否是某个类的实例，或者是某个类的子类的实例。
        // 这个关键字在运行时检查对象的类型，返回一个布尔值。它是进行类型检查和类型转换的重要工具。
        if (modemVisitor instanceof HayesVisitor) {
            ((HayesVisitor) modemVisitor).visit(this);
        } else {
            System.out.println("Only HayesVisitor is allowed to visit Zoom modem");
        }
    }
}
