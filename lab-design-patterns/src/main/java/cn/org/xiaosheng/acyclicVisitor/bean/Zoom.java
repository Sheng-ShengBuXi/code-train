package cn.org.xiaosheng.acyclicVisitor.bean;

import cn.org.xiaosheng.acyclicVisitor.bean.interfaces.Modem;
import cn.org.xiaosheng.acyclicVisitor.visitor.interfaces.ModemVisitor;
import cn.org.xiaosheng.acyclicVisitor.visitor.interfaces.ZoomVisitor;

/**
 * @author XiaoSheng
 * @date 2024/7/31 下午3:40
 */
public class Zoom implements Modem {

    @Override
    public void accept(ModemVisitor modemVisitor) {
        if (modemVisitor instanceof ZoomVisitor) {
            ((ZoomVisitor) modemVisitor).visit(this);
        } else {
            System.out.println("Only ZoomVisitor is allowed to visit Zoom modem");
        }
    }
}
