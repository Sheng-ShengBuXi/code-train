package cn.org.xiaosheng.acyclicVisitor.bean.interfaces;

import cn.org.xiaosheng.acyclicVisitor.visitor.interfaces.ModemVisitor;

/**
 * @author XiaoSheng
 * @date 2024/7/31 下午3:39
 */
public interface Modem {

    public abstract void accept(ModemVisitor modemVisitor);

}
