package cn.org.xiaosheng.acyclicVisitor.visitor;

import cn.org.xiaosheng.acyclicVisitor.bean.Hayes;
import cn.org.xiaosheng.acyclicVisitor.bean.Zoom;
import cn.org.xiaosheng.acyclicVisitor.visitor.interfaces.AllModemVisitor;

/**
 * @author XiaoSheng
 * @date 2024/7/31 下午3:55
 */
public class ConfigureForDosVisitor implements AllModemVisitor {

    @Override
    public void visit(Hayes hayes) {
        System.out.println(hayes + " used with Dos configurator.");
    }

    @Override
    public void visit(Zoom zoom) {
        System.out.println(zoom + " used with Dos configurator.");
    }
}
