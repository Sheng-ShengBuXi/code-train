package cn.org.xiaosheng.acyclicVisitor.visitor;

import cn.org.xiaosheng.acyclicVisitor.bean.Zoom;
import cn.org.xiaosheng.acyclicVisitor.visitor.interfaces.ZoomVisitor;

/**
 * @author XiaoSheng
 * @date 2024/7/31 下午3:56
 */
public class ConfigureForUnixVisitor implements ZoomVisitor {

    @Override
    public void visit(Zoom zoom) {
        System.out.println(zoom + " used with Unix configurator.");
    }
}
