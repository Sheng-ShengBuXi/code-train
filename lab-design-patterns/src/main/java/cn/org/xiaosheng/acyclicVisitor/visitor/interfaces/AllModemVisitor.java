package cn.org.xiaosheng.acyclicVisitor.visitor.interfaces;


/**
 * @author XiaoSheng
 * @date 2024/7/31 下午3:54
 */
public interface AllModemVisitor extends HayesVisitor, ZoomVisitor {
}
