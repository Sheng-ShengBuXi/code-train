package cn.org.xiaosheng.acyclicVisitor.visitor.interfaces;

import cn.org.xiaosheng.acyclicVisitor.bean.Hayes;
import cn.org.xiaosheng.acyclicVisitor.bean.Zoom;

/**
 * @author XiaoSheng
 * @date 2024/7/31 下午3:43
 */
public interface HayesVisitor extends ModemVisitor{
    void visit(Hayes hayes);
}
