package cn.org.xiaosheng.acyclicVisitor.visitor.interfaces;

import cn.org.xiaosheng.acyclicVisitor.bean.Zoom;

/**
 * @author XiaoSheng
 * @date 2024/7/31 下午3:42
 */
public interface ZoomVisitor extends ModemVisitor {

    void visit(Zoom zoom);

}
