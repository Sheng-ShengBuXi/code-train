package cn.org.xiaosheng.adapt.bean;

import cn.org.xiaosheng.adapt.bean.interfaces.RowingBoat;

/**
 * @author XiaoSheng
 * @date 2024/7/31 下午10:21
 */
public class Captain {
    private final RowingBoat rowingBoat;

    // default constructor and setter for rowingBoat
    public Captain(RowingBoat rowingBoat) {
        this.rowingBoat = rowingBoat;
    }

    public void row() {
        rowingBoat.row();
    }
}
