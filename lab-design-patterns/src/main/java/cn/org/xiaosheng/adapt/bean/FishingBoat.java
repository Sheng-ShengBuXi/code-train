package cn.org.xiaosheng.adapt.bean;

/**
 * @author XiaoSheng
 * @date 2024/7/31 下午10:19
 */
public class FishingBoat {

    public void sail() {
        System.out.println("The fishing boat is sailing");
    }

}
