package cn.org.xiaosheng.adapt.bean;

import cn.org.xiaosheng.adapt.bean.interfaces.RowingBoat;

/**
 * @author XiaoSheng
 * @date 2024/7/31 下午10:22
 */
public class FishingBoatAdapter implements RowingBoat {
    private final FishingBoat boat;

    public FishingBoatAdapter() {
        boat = new FishingBoat();
    }

    @Override
    public void row() {
        boat.sail();
    }
}
