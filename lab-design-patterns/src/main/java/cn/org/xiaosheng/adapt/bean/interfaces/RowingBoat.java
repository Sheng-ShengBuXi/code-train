package cn.org.xiaosheng.adapt.bean.interfaces;

/**
 * @author XiaoSheng
 * @date 2024/7/31 下午10:19
 */
public interface RowingBoat {
    void row();
}
