package cn.org.xiaosheng.ambassado;

/**
 * @author XiaoSheng
 * @date 2024/8/1 下午12:46
 */
public class App {
    public static void main(String[] args) {
        Client host1 = new Client();
        Client host2 = new Client();
        try {
            host1.useService(12);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        try {
            host2.useService(73);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
