package cn.org.xiaosheng.ambassado;

/**
 * @author XiaoSheng
 * @date 2024/8/1 下午12:46
 */
public class Client {

    private final ServiceAmbassador serviceAmbassador = new ServiceAmbassador();

    long useService(int value) throws Exception {
        long result = serviceAmbassador.doRemoteFunction(value);
        System.out.println("Service result: " + result);
        return result;
    }

}
