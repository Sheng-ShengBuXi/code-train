package cn.org.xiaosheng.ambassado;

import cn.org.xiaosheng.ambassado.interfaces.RemoteServiceInterface;

/**
 * @author XiaoSheng
 * @date 2024/8/1 下午12:25
 */
public class RemoteService implements RemoteServiceInterface {

    private static RemoteService service = null;


    public static synchronized RemoteService getRemoteService() {
        if (service == null) {
            return new RemoteService();
        } else {
            return service;
        }
    }

    public RemoteService() {

    }

    @Override
    public long doRemoteFunction(int value) throws Exception {
        long waitTime = (long) Math.floor(Math.random() * 1000);
        try{
            Thread.sleep(waitTime);
        }catch (InterruptedException e){
            e.printStackTrace();
            System.out.println("Thread sleep interrupted" + e.getMessage());
        }
        return waitTime >= 200 ? value * 10 : -1;
    }
}
