package cn.org.xiaosheng.ambassado.interfaces;

/**
 * @author XiaoSheng
 * @date 2024/8/1 下午12:24
 */
public interface RemoteServiceInterface {

    public long doRemoteFunction(int value) throws Exception;

}
