package cn.org.xiaosheng.antiCorruptionLayer;

import java.util.Optional;

/**
 * @author XiaoSheng
 * @date 2024/8/1 下午1:27
 */
public class AntiCorruptionLayer {
//    @Autowired
//    private ModernShop modernShop;
//
//    @Autowired
//    private LegacyShop legacyShop;

    private LegacyShop legacyShop;

//    private ModernShop modernShop;
//
//    public AntiCorruptionLayer(ModernOrder modernOrder, LegacyOrder legacyOrder) {
//        this.legacyOrder = legacyOrder;
//        this.modernOrder = modernOrder;
//    }


    public Optional<LegacyOrder> findOrderInModernSystem(String id) {
//        return modernShop.findOrder(id).map(o -> /* map to legacyOrder*/);
        return Optional.ofNullable(new LegacyOrder().setId(id).setItem("xiaosheng").setCustomer("xiaosheng this home!"));
    }


    public Optional<ModernOrder> findOrderInLegacySystem(String id) {
//        return legacyShop.findOrder(id).map(o -> /* map to modernOrder*/);
        return Optional.ofNullable(new ModernOrder().setId(id).setExtra("tongxiao").setCustomer(new Customer()));
    }

}
