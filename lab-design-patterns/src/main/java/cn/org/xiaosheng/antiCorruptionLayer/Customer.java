package cn.org.xiaosheng.antiCorruptionLayer;

/**
 * @author XiaoSheng
 * @date 2024/8/1 下午1:26
 */
public class Customer {
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
