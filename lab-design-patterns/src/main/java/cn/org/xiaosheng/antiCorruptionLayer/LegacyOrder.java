package cn.org.xiaosheng.antiCorruptionLayer;

/**
 * @author XiaoSheng
 * @date 2024/8/1 下午1:24
 */

public class LegacyOrder {
    private String id;
    private String customer;
    private String item;
    private String qty;
    private String price;


    public String getId() {
        return id;
    }

    public LegacyOrder setId(String id) {
        this.id = id;
        return this;
    }

    public String getCustomer() {
        return customer;
    }

    public LegacyOrder setCustomer(String customer) {
        this.customer = customer;
        return this;
    }

    public String getItem() {
        return item;
    }

    public LegacyOrder setItem(String item) {
        this.item = item;
        return this;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
