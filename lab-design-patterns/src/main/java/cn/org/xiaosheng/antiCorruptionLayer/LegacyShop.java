package cn.org.xiaosheng.antiCorruptionLayer;

import java.util.Optional;

/**
 * @author XiaoSheng
 * @date 2024/8/1 下午1:33
 */
public class LegacyShop {


    private AntiCorruptionLayer acl;

    public void placeOrder(LegacyOrder legacyOrder) {

        String id = legacyOrder.getId();

        Optional<LegacyOrder> orderInModernSystem = acl.findOrderInModernSystem(id);

        if (orderInModernSystem.isPresent()) {
            // order is already in the modern system
        } else {
            // place order in the current system
        }
    }
}
