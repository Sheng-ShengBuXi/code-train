package cn.org.xiaosheng.antiCorruptionLayer;

/**
 * @author XiaoSheng
 * @date 2024/8/1 下午1:25
 */
public class ModernOrder {

    private String id;
    private Customer customer;

    private Shipment shipment;

    private String extra;

    public String getId() {
        return id;
    }

    public ModernOrder setId(String id) {
        this.id = id;
        return this;
    }

    public Customer getCustomer() {
        return customer;
    }

    public ModernOrder setCustomer(Customer customer) {
        this.customer = customer;
        return this;
    }

    public Shipment getShipment() {
        return shipment;
    }

    public ModernOrder setShipment(Shipment shipment) {
        this.shipment = shipment;
        return this;
    }

    public String getExtra() {
        return extra;
    }

    public ModernOrder setExtra(String extra) {
        this.extra = extra;
        return this;
    }
}
