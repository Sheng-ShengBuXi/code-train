package cn.org.xiaosheng.antiCorruptionLayer;

/**
 * @author XiaoSheng
 * @date 2024/8/1 下午1:26
 */
public class Shipment {
    private String item;
    private String qty;
    private String price;

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
