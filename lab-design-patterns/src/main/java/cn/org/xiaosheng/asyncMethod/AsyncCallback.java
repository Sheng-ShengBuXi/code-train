package cn.org.xiaosheng.asyncMethod;

/**
 * @author XiaoSheng
 * @date 2024/8/2 上午10:34
 */
public interface AsyncCallback<T> {

    void onComplete(T value);

    void onError(Exception ex);

}
