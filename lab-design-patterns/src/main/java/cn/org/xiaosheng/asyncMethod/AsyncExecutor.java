package cn.org.xiaosheng.asyncMethod;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

/**
 * @author XiaoSheng
 * @date 2024/8/2 上午10:36
 */
public interface AsyncExecutor {

    <T> AsyncResult<T> startProcess(Callable<T> task);

    <T> AsyncResult<T> startProcess(Callable<T> task, AsyncCallback<T> callback);

    <T> T endProcess(AsyncResult<T> asyncResult) throws ExecutionException, InterruptedException;

}
