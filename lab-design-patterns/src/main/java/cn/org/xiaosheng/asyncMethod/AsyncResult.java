package cn.org.xiaosheng.asyncMethod;

import java.util.concurrent.ExecutionException;

/**
 * @author XiaoSheng
 * @date 2024/8/2 上午10:31
 */
public interface AsyncResult<T> {

    boolean isCompleted();

    T getValue() throws ExecutionException;

    void await() throws InterruptedException;
}
