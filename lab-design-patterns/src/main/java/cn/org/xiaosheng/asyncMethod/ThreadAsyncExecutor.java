package cn.org.xiaosheng.asyncMethod;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

/**
 * @author XiaoSheng
 * @date 2024/8/2 上午10:45
 */
public class ThreadAsyncExecutor implements AsyncExecutor {

    @Override
    public <T> AsyncResult<T> startProcess(Callable<T> task) {
        return startProcess(task, null);
    }

    @Override
    public <T> AsyncResult<T> startProcess(Callable<T> task, AsyncCallback<T> callback) {
//        CompletableResult result = new CompletableResult<>(callback);
        return null;
    }

    @Override
    public <T> T endProcess(AsyncResult<T> asyncResult) throws ExecutionException, InterruptedException {
        return null;
    }
}
