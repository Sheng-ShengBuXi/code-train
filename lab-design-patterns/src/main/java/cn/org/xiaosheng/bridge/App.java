package cn.org.xiaosheng.bridge;

/**
 * @author XiaoSheng
 * @date 2024/8/2 下午1:18
 */
public class App {

    public static void main(String[] args) {
        System.out.println("The knight receives an enchanted sword.");
        Sword enchantedSword = new Sword(new SoulEatingEnchantment());
        enchantedSword.wield();
        enchantedSword.swing();
        enchantedSword.unwield();

        System.out.println("The valkyrie receives an enchanted hammer.");
        Hammer hammer = new Hammer(new FlyingEnchantment());
        hammer.wield();
        hammer.swing();
        hammer.unwield();
    }

}
