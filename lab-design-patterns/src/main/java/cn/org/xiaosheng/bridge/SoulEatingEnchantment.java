package cn.org.xiaosheng.bridge;

import cn.org.xiaosheng.bridge.interfaces.Enchantment;

/**
 * @author XiaoSheng
 * @date 2024/8/2 下午1:17
 */
public class SoulEatingEnchantment implements Enchantment {

    @Override
    public void onActivate() {
        System.out.println("The item spreads bloodlust.");
    }

    @Override
    public void apply() {
        System.out.println("The item eats the soul of enemies.");
    }

    @Override
    public void onDeactivate() {
        System.out.println("Bloodlust slowly disappears.");
    }


}
