package cn.org.xiaosheng.bridge.interfaces;

/**
 * @author XiaoSheng
 * @date 2024/8/2 下午1:14
 */
public interface Enchantment {

    void onActivate();

    void apply();

    void onDeactivate();

}
