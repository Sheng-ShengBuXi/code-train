package cn.org.xiaosheng.bridge.interfaces;

/**
 * @author XiaoSheng
 * @date 2024/8/2 下午1:13
 */
public interface Weapon {

    void wield();

    void swing();

    void unwield();

    Enchantment getEnchantment();

}
