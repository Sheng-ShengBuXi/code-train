package cn.org.xiaosheng.businessDelegate;

/**
 * @author XiaoSheng
 * @date 2024/8/3 下午7:59
 */
public class App {

    public static void main(String[] args) {
        // prepare the objects
        BusinessDelegate businessDelegate = new BusinessDelegate();
        BusinessLookup businessLookup = new BusinessLookup();
        businessLookup.setNetflixService(new NetflixService());
        businessLookup.setYouTubeService(new YouTubeService());
        businessDelegate.setLookupService(businessLookup);

        // create the client and use the business delegate
        MobileClient client = new MobileClient(businessDelegate);
        client.playbackMovie("Die Hard 2");
        client.playbackMovie("Maradona: The Greatest Ever");
    }

}
