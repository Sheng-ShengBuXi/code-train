package cn.org.xiaosheng.businessDelegate;

import cn.org.xiaosheng.businessDelegate.interfaces.VideoStreamingService;

/**
 * @author XiaoSheng
 * @date 2024/8/3 下午7:14
 */
public class BusinessDelegate {

    private BusinessLookup lookupService;

    public void playbackMovie(String movie) {
        VideoStreamingService videoStreamingService = lookupService.getBusinessService(movie);
        videoStreamingService.doProcessing();
    }

    public void setLookupService(BusinessLookup lookupService) {
        this.lookupService = lookupService;
    }
}
