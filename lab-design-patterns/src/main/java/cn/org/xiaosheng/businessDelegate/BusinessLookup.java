package cn.org.xiaosheng.businessDelegate;

import cn.org.xiaosheng.businessDelegate.interfaces.VideoStreamingService;

import java.util.Locale;

/**
 * @author XiaoSheng
 * @date 2024/8/3 下午7:13
 */
public class BusinessLookup {

    private NetflixService netflixService;
    private YouTubeService youTubeService;

    public VideoStreamingService getBusinessService(String movie) {
        if (movie.toLowerCase(Locale.ROOT).contains("die hard")) {
            return netflixService;
        } else {
            return youTubeService;
        }
    }

    public NetflixService getNetflixService() {
        return netflixService;
    }

    public void setNetflixService(NetflixService netflixService) {
        this.netflixService = netflixService;
    }

    public YouTubeService getYouTubeService() {
        return youTubeService;
    }

    public void setYouTubeService(YouTubeService youTubeService) {
        this.youTubeService = youTubeService;
    }
}
