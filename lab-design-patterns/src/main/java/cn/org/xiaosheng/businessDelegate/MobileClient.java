package cn.org.xiaosheng.businessDelegate;

/**
 * @author XiaoSheng
 * @date 2024/8/3 下午7:14
 */
public class MobileClient {

    private final BusinessDelegate businessDelegate;

    public MobileClient(BusinessDelegate businessDelegate) {
        this.businessDelegate = businessDelegate;
    }

    public void playbackMovie(String movie) {
        businessDelegate.playbackMovie(movie);
    }

}
