package cn.org.xiaosheng.businessDelegate;

import cn.org.xiaosheng.businessDelegate.interfaces.VideoStreamingService;

/**
 * @author XiaoSheng
 * @date 2024/8/3 下午7:12
 */
public class NetflixService implements VideoStreamingService {

    @Override
    public void doProcessing() {
        System.out.println("NetflixService is now processing");
    }
}
