package cn.org.xiaosheng.businessDelegate.interfaces;

/**
 * @author XiaoSheng
 * @date 2024/8/3 下午7:12
 */
public interface VideoStreamingService {

    void doProcessing();

}
