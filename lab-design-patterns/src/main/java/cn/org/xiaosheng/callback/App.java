package cn.org.xiaosheng.callback;

/**
 * @author XiaoSheng
 * @date 2024/8/5 上午11:26
 */
public class App {

    public static void main(String[] args) {
        SimpleTask simpleTask = new SimpleTask();
        simpleTask.executeWith(() -> System.out.println("I'm done now."));
    }

}
