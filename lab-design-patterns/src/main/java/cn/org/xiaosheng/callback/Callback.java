package cn.org.xiaosheng.callback;

/**
 * @author XiaoSheng
 * @date 2024/8/5 上午11:03
 */
public interface Callback {
    void call();
}
