package cn.org.xiaosheng.callback;

/**
 * @author XiaoSheng
 * @date 2024/8/5 上午11:04
 */
public class SimpleTask extends Task {

    @Override
    public void execute() {
        System.out.println("Perform some important activity and after call the callback method.");
    }

}
