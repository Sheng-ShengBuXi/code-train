package cn.org.xiaosheng.callback;

import java.util.Optional;

/**
 * @author XiaoSheng
 * @date 2024/8/5 上午11:04
 */
public abstract class Task {

    final void executeWith(Callback callback) {
        execute();
        Optional.ofNullable(callback).ifPresent(Callback::call);
    }

    public abstract void execute();

}
