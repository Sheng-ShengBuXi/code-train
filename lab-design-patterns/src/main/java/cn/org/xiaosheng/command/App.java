package cn.org.xiaosheng.command;

import cn.org.xiaosheng.command.interfaces.Command;

/**
 * @author XiaoSheng
 * @date 2024/8/6 下午6:41
 */
public class App {

    public static void main(String[] args) {
        // Create receiver
        Light livingRoomLight = new Light();

        // Create commands
        Command lightOn = new LightOnCommand(livingRoomLight);
        Command lightOff = new LightOffCommand(livingRoomLight);

        // Create invoker
        RemoteControl remote = new RemoteControl();

        // Turn on the light
        remote.setCommand(lightOn);
        remote.pressButton();

        // Turn off the light
        remote.setCommand(lightOff);
        remote.pressButton();
    }

}
