package cn.org.xiaosheng.command;

/**
 * @author XiaoSheng
 * @date 2024/8/6 下午6:21
 */
public class Light {

    public void turnOn() {
        System.out.println("The light is on.");
    }

    public void turnOff() {
        System.out.println("The light is off.");
    }

}
