package cn.org.xiaosheng.command;

import cn.org.xiaosheng.command.interfaces.Command;

/**
 * @author XiaoSheng
 * @date 2024/8/6 下午6:38
 */
public class LightOffCommand implements Command {

    private Light light;

    public LightOffCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        light.turnOff();
    }

}
