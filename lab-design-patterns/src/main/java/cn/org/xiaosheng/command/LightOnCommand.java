package cn.org.xiaosheng.command;

import cn.org.xiaosheng.command.interfaces.Command;

/**
 * @author XiaoSheng
 * @date 2024/8/6 下午6:20
 */
public class LightOnCommand implements Command {

    private Light light;

    public LightOnCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        light.turnOn();
    }

}
