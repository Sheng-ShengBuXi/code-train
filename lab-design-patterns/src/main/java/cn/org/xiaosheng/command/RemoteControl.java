package cn.org.xiaosheng.command;

import cn.org.xiaosheng.command.interfaces.Command;

/**
 * @author XiaoSheng
 * @date 2024/8/6 下午6:22
 */
public class RemoteControl {

    private Command command;

    public void setCommand(Command command) {
        this.command = command;
    }

    public void pressButton() {
        command.execute();
    }

}
