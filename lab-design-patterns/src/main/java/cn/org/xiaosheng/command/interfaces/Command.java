package cn.org.xiaosheng.command.interfaces;

/**
 * @author XiaoSheng
 * @date 2024/8/6 下午6:19
 */
public interface Command {

    void execute();

}
