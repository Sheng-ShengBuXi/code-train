package cn.org.xiaosheng.composite;

/**
 * @author XiaoSheng
 * @date 2024/8/6 下午7:25
 */
public class App {
    public static void main(String[] args) {
        Messenger messenger = new Messenger();

        System.out.println("Message from the orcs: ");
        messenger.messageFromOrcs().print();

        System.out.println("Message from the elves: ");
        messenger.messageFromElves().print();
    }
}
