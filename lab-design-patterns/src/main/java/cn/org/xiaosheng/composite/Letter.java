package cn.org.xiaosheng.composite;

/**
 * @author XiaoSheng
 * @date 2024/8/6 下午7:02
 */
public class Letter extends LetterComposite {

    private final char character;

    public Letter(char c) {
        this.character = c;
    }

    @Override
    protected void printThisBefore() {
        System.out.print(character);
    }

}
