package cn.org.xiaosheng.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author XiaoSheng
 * @date 2024/8/6 下午7:10
 */
public class Messenger {

    LetterComposite messageFromOrcs() {

        List<Word> words = new ArrayList<>();

        Word[] words1 = new Word[]{new Word('W', 'h', 'e', 'r', 'e'),
                new Word('t', 'h', 'e', 'r', 'e'),
                new Word('i', 's'),
                new Word('a'),
                new Word('w', 'h', 'i', 'p'),
                new Word('t', 'h', 'e', 'r', 'e'),
                new Word('i', 's'),
                new Word('a'),
                new Word('w', 'a', 'y')};

        Collections.addAll(words, words1);


        return new Sentence(words);

    }

    LetterComposite messageFromElves() {

        List<Word> words =new ArrayList<>();


        Word[] words1 = new Word[]{
                new Word('M', 'u', 'c', 'h'),
                new Word('w', 'i', 'n', 'd'),
                new Word('p', 'o', 'u', 'r', 's'),
                new Word('f', 'r', 'o', 'm'),
                new Word('y', 'o', 'u', 'r'),
                new Word('m', 'o', 'u', 't', 'h')
        };
        Collections.addAll(words, words1);

        return new Sentence(words);
    }


}
