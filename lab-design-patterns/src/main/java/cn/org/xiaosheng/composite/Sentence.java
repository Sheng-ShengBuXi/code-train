package cn.org.xiaosheng.composite;

import java.util.List;

/**
 * @author XiaoSheng
 * @date 2024/8/6 下午7:06
 */
public class Sentence extends LetterComposite {

    public Sentence(List<Word> words) {
        words.forEach(this::add);
    }

    @Override
    protected void printThisAfter() {
        System.out.print(".");
    }

}
