package cn.org.xiaosheng.composite;

import java.util.List;

/**
 * @author XiaoSheng
 * @date 2024/8/6 下午7:03
 */
public class Word extends LetterComposite {

    public Word(List<Letter> letters) {
        letters.forEach(this::add);
    }

    public Word(char... letters) {
        for (char letter : letters) {
            this.add(new Letter(letter));
        }
    }

    @Override
    protected void printThisBefore() {
        System.out.print(" ");
    }

}
