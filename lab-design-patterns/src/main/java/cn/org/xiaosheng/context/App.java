package cn.org.xiaosheng.context;

import java.util.List;

/**
 * @author XiaoSheng
 * @date 2024/8/7 下午12:26
 */
public class App {

    private static final String SERVICE = "SERVICE";

    public static void main(String[] args) {

        LayerA layerA = new LayerA();
        layerA.addAccountInfo(SERVICE);

        logContext(layerA.getContext());

        LayerB layerB = new LayerB(layerA);
        layerB.addSessionInfo(SERVICE);

        logContext(layerB.getContext());

        LayerC layerC = new LayerC(layerB);
        layerC.addSearchInfo(SERVICE);

        logContext(layerC.getContext());

    }

    private static void logContext(ServiceContext context) {
        System.out.println("Context = " + context);
    }

}
