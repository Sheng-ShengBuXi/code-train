package cn.org.xiaosheng.context;

/**
 * @author XiaoSheng
 * @date 2024/8/7 下午12:06
 */
public class LayerA {


    private static ServiceContext context;

    public LayerA() {
        context = ServiceContextFactory.createContext();
    }

    public void addAccountInfo(String accountService) {
        context.setAccountService(accountService);
    }

    public static ServiceContext getContext() {
        return context;
    }
}
