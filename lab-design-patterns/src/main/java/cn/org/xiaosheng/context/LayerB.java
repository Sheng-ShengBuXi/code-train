package cn.org.xiaosheng.context;

/**
 * @author XiaoSheng
 * @date 2024/8/7 下午12:08
 */
public class LayerB {

    private static ServiceContext context;

    public LayerB(LayerA layerA) {
        this.context = layerA.getContext();
    }

    public void addSessionInfo(String sessionService) {
        context.setSessionService(sessionService);
    }

    public static ServiceContext getContext() {
        return context;
    }
}
