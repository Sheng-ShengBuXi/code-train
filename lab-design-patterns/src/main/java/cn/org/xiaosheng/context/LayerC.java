package cn.org.xiaosheng.context;

/**
 * @author XiaoSheng
 * @date 2024/8/7 下午12:24
 */
public class LayerC {

    public static ServiceContext context;

    public LayerC(LayerB layerB) {
        this.context = layerB.getContext();
    }

    public void addSearchInfo(String searchService) {
        context.setSearchService(searchService);
    }

    public static ServiceContext getContext() {
        return context;
    }
}
