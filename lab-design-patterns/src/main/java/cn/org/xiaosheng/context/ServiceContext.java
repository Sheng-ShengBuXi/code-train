package cn.org.xiaosheng.context;

/**
 * @author XiaoSheng
 * @date 2024/8/7 下午12:05
 */
public class ServiceContext {

    public String accountService;
    public String sessionService;
    public String searchService;

    public String getAccountService() {
        return accountService;
    }

    public void setAccountService(String accountService) {
        this.accountService = accountService;
    }

    public String getSessionService() {
        return sessionService;
    }

    public void setSessionService(String sessionService) {
        this.sessionService = sessionService;
    }

    public String getSearchService() {
        return searchService;
    }

    public void setSearchService(String searchService) {
        this.searchService = searchService;
    }
}
