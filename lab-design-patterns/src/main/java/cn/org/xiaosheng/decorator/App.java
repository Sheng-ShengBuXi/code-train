package cn.org.xiaosheng.decorator;

/**
 * @author XiaoSheng
 * @date 2024/8/8 上午11:16
 */
public class App {
    public static void main(String[] args) {
        SimpleTroll simpleTroll = new SimpleTroll();
        ClubbedTroll clubbedTroll = new ClubbedTroll(simpleTroll);
        simpleTroll.attack();
        clubbedTroll.attack();
    }
}
