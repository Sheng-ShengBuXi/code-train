package cn.org.xiaosheng.decorator;

/**
 * @author XiaoSheng
 * @date 2024/8/8 上午11:14
 */
public class ClubbedTroll implements Troll {

    private final Troll decorated;

    public ClubbedTroll(Troll decorated) {
        this.decorated = decorated;
    }

    @Override
    public void attack() {
        decorated.attack();
        System.out.println("The troll swings at you with a club!");
    }

    @Override
    public int getAttackPower() {
        return decorated.getAttackPower() + 10;
    }

    @Override
    public void fleeBattle() {
        decorated.fleeBattle();
    }

}
