package cn.org.xiaosheng.decorator;

/**
 * @author XiaoSheng
 * @date 2024/8/8 上午11:13
 */
public class SimpleTroll implements Troll {

    @Override
    public void attack() {
        System.out.println("The troll tries to grab you!");
    }

    @Override
    public int getAttackPower() {
        return 10;
    }

    @Override
    public void fleeBattle() {
        System.out.println("The troll shrieks in horror and runs away!");
    }

}
