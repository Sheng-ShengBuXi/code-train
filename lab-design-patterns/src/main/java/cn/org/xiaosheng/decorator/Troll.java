package cn.org.xiaosheng.decorator;

/**
 * @author XiaoSheng
 * @date 2024/8/8 上午11:10
 */
public interface Troll {

    void attack();

    int getAttackPower();

    void fleeBattle();

}
