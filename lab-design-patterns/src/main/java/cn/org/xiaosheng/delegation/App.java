package cn.org.xiaosheng.delegation;

/**
 * @author XiaoSheng
 * @date 2024/8/8 上午11:38
 */
public class App {

    private static final String MESSAGE_TO_PRINT = "hello world";

    public static void main(String[] args) {

        PrinterController hpPrinterController = new PrinterController(new HpPrinter());
        PrinterController canonPrinterController = new PrinterController(new CanonPrinter());
        PrinterController epsonPrinterController = new PrinterController(new EpsonPrinter());

        hpPrinterController.print(MESSAGE_TO_PRINT);
        canonPrinterController.print(MESSAGE_TO_PRINT);
        epsonPrinterController.print(MESSAGE_TO_PRINT);

    }

}
