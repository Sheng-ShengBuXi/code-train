package cn.org.xiaosheng.delegation;

/**
 * @author XiaoSheng
 * @date 2024/8/8 上午11:29
 */
public class CanonPrinter implements Printer {


    @Override
    public void print(String message) {
        System.out.println("Canon Printer : " + message);
    }
}
