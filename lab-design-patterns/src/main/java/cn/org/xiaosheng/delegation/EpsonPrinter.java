package cn.org.xiaosheng.delegation;

/**
 * @author XiaoSheng
 * @date 2024/8/8 上午11:30
 */
public class EpsonPrinter implements Printer{

    @Override
    public void print(String message) {
        System.out.println("Epson Printer : " + message);
    }
}
