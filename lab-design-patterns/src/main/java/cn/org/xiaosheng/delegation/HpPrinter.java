package cn.org.xiaosheng.delegation;

/**
 * @author XiaoSheng
 * @date 2024/8/8 上午11:31
 */
public class HpPrinter implements Printer{

    @Override
    public void print(String message) {
        System.out.println("Hp Printer : " + message);
    }
}
