package cn.org.xiaosheng.delegation;

/**
 * @author XiaoSheng
 * @date 2024/8/8 上午11:28
 */
public interface Printer {

    void print(final String message);

}
