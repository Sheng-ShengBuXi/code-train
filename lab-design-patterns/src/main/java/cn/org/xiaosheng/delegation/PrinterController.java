package cn.org.xiaosheng.delegation;

/**
 * @author XiaoSheng
 * @date 2024/8/8 上午11:32
 */
public class PrinterController implements Printer {

    private final Printer printer;

    public PrinterController(Printer printer) {
        this.printer = printer;
    }

    @Override
    public void print(String message) {
        printer.print(message);
    }

}
