package cn.org.xiaosheng.doubleDispatch;

/**
 * @author XiaoSheng
 * @date 2024/8/9 上午10:24
 */
public abstract class GameObject {

    public abstract void collision(GameObject gameObject);

}
