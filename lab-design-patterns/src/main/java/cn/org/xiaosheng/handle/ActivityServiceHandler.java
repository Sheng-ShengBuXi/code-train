package cn.org.xiaosheng.handle;

import java.util.concurrent.CompletableFuture;

// 实现商品活动补全节点
public class ActivityServiceHandler implements ServiceHandler {
    @Override
    public CompletableFuture<Void> handle(ProductInfo productInfo) {
        CompletableFuture<Void> future = new CompletableFuture<>();
        // 模拟异步调用外部服务，修改商品信息对象
        CompletableFuture.runAsync(() -> {
            // 补充商品活动数据
            productInfo.setActivity("...");
            future.complete(null);
        });
        return future;
    }
}
