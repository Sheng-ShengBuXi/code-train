package cn.org.xiaosheng.handle;

import java.util.concurrent.CompletableFuture;

public class BasicInfoServiceHandler implements ServiceHandler{

    @Override
    public CompletableFuture<Void> handle(ProductInfo productInfo) {
        CompletableFuture<Void> future = new CompletableFuture<>();
        CompletableFuture.runAsync(() -> {
            // 补充商品基础信息数据
            productInfo.setBasicInfo("基本介绍");
            future.complete(null);
        });
        return future;
    }
}
