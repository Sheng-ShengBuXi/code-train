package cn.org.xiaosheng.handle;

import java.util.concurrent.CompletableFuture;

public class InventoryServiceHandler implements ServiceHandler {
    @Override
    public CompletableFuture<Void> handle(ProductInfo productInfo) {
        CompletableFuture<Void> future = new CompletableFuture<>();
        // 模拟异步调用外部服务，修改商品信息对象
        CompletableFuture.runAsync(() -> {
            // 补充商品库存数据
            productInfo.setInventory(100);
            future.complete(null);
        });
        return future;
    }
}
