package cn.org.xiaosheng.handle;

import java.util.concurrent.CompletableFuture;

public // 实现商品价格补全节点
class PriceServiceHandler implements ServiceHandler {
    @Override
    public CompletableFuture<Void> handle(ProductInfo productInfo) {
        CompletableFuture<Void> future = new CompletableFuture<>();
        // 模拟异步调用外部服务，修改商品信息对象
        CompletableFuture.runAsync(() -> {
            // 补充商品价格数据
            productInfo.setPrice(100.0);
            future.complete(null);
        });
        return future;
    }
}

