package cn.org.xiaosheng.handle;

import java.util.concurrent.CompletableFuture;

public interface ServiceHandler {

    CompletableFuture<Void> handle(ProductInfo productInfo);

}
