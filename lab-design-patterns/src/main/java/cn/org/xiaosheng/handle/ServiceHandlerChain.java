package cn.org.xiaosheng.handle;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class ServiceHandlerChain {
    private List<ServiceHandler> handlers = new ArrayList<>();

    public void addHandler(ServiceHandler handler) {
        handlers.add(handler);
    }

    public ProductInfo execute(ProductInfo productInfo) {
        CompletableFuture<Void>[] futures = handlers.stream()
                .map(handler -> handler.handle(productInfo))
                .toArray(CompletableFuture[]::new);
        //等待所有的异步补全任务处理完成
        CompletableFuture<Void> allOfFuture = CompletableFuture.allOf(futures);
        allOfFuture.join(); // 等待所有异步操作完成
        return productInfo;
    }
}
