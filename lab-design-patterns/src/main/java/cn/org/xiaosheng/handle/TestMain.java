package cn.org.xiaosheng.handle;

public class TestMain {
    public static void main(String[] args) {
        // 创建商品信息对象
        ProductInfo productInfo = new ProductInfo();

        // 创建责任链
        ServiceHandlerChain chain = new ServiceHandlerChain();
        chain.addHandler(new BasicInfoServiceHandler());
        chain.addHandler(new PriceServiceHandler());
        chain.addHandler(new ActivityServiceHandler());
        chain.addHandler(new InventoryServiceHandler());

        // 执行责任链
        ProductInfo result = chain.execute(productInfo);

        System.out.println(result.toString());
    }
}
