package cn.org.xiaosheng.observers;

/**
 * @author XiaoSheng
 * @date 2024/8/13 下午12:23
 */
public class App {

    public static void main(String[] args) {
        ConcreteSubject concreteSubject = new ConcreteSubject();
        Observer observer1 = new ConcreteObserver("Observer 1");
        Observer observer2 = new ConcreteObserver("Observer 2");
        concreteSubject.registerObserver(observer2);
        concreteSubject.registerObserver(observer1);
        concreteSubject.setMessage("Hello XiaoSheng!");
        concreteSubject.setMessage("总是以为勇敢的水手是真正的男儿");
    }

}
