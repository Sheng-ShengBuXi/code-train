package cn.org.xiaosheng.observers;

/**
 * @author XiaoSheng
 * @date 2024/8/13 上午11:47
 */
public class ConcreteObserver implements Observer {

    private String name;

    public ConcreteObserver(String name) {
        this.name = name;
    }

    @Override
    public void update(String message) {
        System.out.println(name + " received message: " + message);
    }
}
