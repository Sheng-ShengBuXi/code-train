package cn.org.xiaosheng.observers;

import java.util.ArrayList;
import java.util.List;

/**
 * @author XiaoSheng
 * @date 2024/8/13 上午11:42
 */
public class ConcreteSubject implements Subject{

    private List<Observer> observers;
    private String message;

    public ConcreteSubject() {
        this.observers = new ArrayList<>();
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        for (Observer o :observers) {
            o.update(message);
        }
    }

    public void setMessage(String message) {
        this.message = message;
        notifyObservers();
    }

}
