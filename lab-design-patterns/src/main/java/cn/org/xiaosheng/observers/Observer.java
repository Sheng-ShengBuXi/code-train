package cn.org.xiaosheng.observers;

/**
 * @author XiaoSheng
 * @date 2024/8/13 上午11:42
 */
public interface Observer {

    void update(String message);

}
