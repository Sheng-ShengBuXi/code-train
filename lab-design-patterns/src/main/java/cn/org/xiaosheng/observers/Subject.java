package cn.org.xiaosheng.observers;

/**
 * @author XiaoSheng
 * @date 2024/8/13 上午11:32
 */
public interface Subject {

    void registerObserver(Observer o);
    void removeObserver(Observer o);
    void notifyObservers();

}
