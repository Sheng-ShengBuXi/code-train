package cn.org.xiaosheng.retry;

/**
 * @author XiaoSheng
 * @date 2024/8/14 下午3:54
 */
public interface BusinessOperation<T> {

    T perform() throws Exception;

}
