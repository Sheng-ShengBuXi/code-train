package cn.org.xiaosheng.singleton;

/**
 * @author XiaoSheng
 * @date 2024/8/7 下午4:26
 */
public class Client {

    public static void main(String[] args) {
        // 获取单例实例
        Singleton singleton = Singleton.INSTANCEss;
        singleton.doSomething();

        // 确认同一实例
        Singleton anotherInstance = Singleton.INSTANCEss;
        System.out.println(singleton == anotherInstance); // 输出 true
    }

}
