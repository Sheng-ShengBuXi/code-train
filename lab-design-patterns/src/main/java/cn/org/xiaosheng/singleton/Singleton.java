package cn.org.xiaosheng.singleton;

/**
 * @author XiaoSheng
 * @date 2024/8/7 下午4:25
 */
public enum Singleton {
    INSTANCEss;

    // 可以在这里定义单例需要的方法
    public void doSomething() {
        System.out.println("Doing something...");
    }
}
