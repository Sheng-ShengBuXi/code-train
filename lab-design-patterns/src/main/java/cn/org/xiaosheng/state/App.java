package cn.org.xiaosheng.state;

/**
 * @author XiaoSheng
 * @date 2024/8/3 下午10:49
 */
public class App {
    public static void main(String[] args) {
        // 初始化上下文，并设置初始状态为 ConcreteStateA
        Context context = new Context(new ConcreteStateA());

        // 触发请求，具体状态类将处理请求并切换状态
        context.request(); // 输出: State A handling request.
        context.request(); // 输出: State B handling request.
        context.request(); // 输出: State A handling request.
        context.request(); // 输出: State B handling request.
    }
}
