package cn.org.xiaosheng.state;

import cn.org.xiaosheng.state.interfaces.State;

/**
 * @author XiaoSheng
 * @date 2024/8/3 下午10:36
 */
public class ConcreteStateA implements State {

    @Override
    public void handle(Context context) {
        System.out.println("State A handling request.");
        context.setState(new ConcreteStateB());
    }

}
