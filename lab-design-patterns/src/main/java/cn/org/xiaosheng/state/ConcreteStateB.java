package cn.org.xiaosheng.state;

import cn.org.xiaosheng.state.interfaces.State;

/**
 * @author XiaoSheng
 * @date 2024/8/3 下午10:40
 */
public class ConcreteStateB implements State {

    @Override
    public void handle(Context context) {
        System.out.println("State B handling request.");
        context.setState(new ConcreteStateA());
    }
}
