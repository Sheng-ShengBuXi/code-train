package cn.org.xiaosheng.state;

import cn.org.xiaosheng.state.interfaces.State;

/**
 * @author XiaoSheng
 * @date 2024/8/3 下午10:35
 */
public class Context {

    private State state;

    public Context(State state) {
        this.state = state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void request() {
        state.handle(this);
    }

}
