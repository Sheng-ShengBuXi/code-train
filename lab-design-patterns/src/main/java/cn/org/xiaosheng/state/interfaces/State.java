package cn.org.xiaosheng.state.interfaces;

import cn.org.xiaosheng.state.Context;

/**
 * @author XiaoSheng
 * @date 2024/8/3 下午10:35
 */
public interface State {

    void handle(Context context);

}
