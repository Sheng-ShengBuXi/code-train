package cn.org.xiaosheng.strategy;

public interface PaymentStrategy {
    void pay(double amount);
}
