package cn.org.xiaosheng.strategy;

import java.util.HashMap;
import java.util.Map;

public class PaymentStrategyFactory {

    Map<String, PaymentStrategy> strategyMap = new HashMap<String, PaymentStrategy>();

    public void registerStrategy(String strategyName, PaymentStrategy paymentStrategy) {
        strategyMap.put(strategyName, paymentStrategy);
    }

    public PaymentStrategy getStrategy(String strategyName) {
        return strategyMap.get(strategyName);
    }

}
