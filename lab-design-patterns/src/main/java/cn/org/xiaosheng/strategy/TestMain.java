package cn.org.xiaosheng.strategy;

public class TestMain {

    public static void main(String[] args) {

        PaymentStrategyFactory factory = new PaymentStrategyFactory();
        factory.registerStrategy("weChatPay", new WeChatPaymentStrategy());
        factory.registerStrategy("alibaPay", new AlipayPaymentStrategy());

        PaymentStrategy weChatPay = factory.getStrategy("weChatPay");
        PaymentStrategy alibaPay = factory.getStrategy("alibaPay");

        weChatPay.pay(22);
        alibaPay.pay(33300);


    }

}
