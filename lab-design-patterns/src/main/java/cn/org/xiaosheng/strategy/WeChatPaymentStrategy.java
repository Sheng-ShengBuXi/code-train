package cn.org.xiaosheng.strategy;

public class WeChatPaymentStrategy implements PaymentStrategy {

    @Override
    public void pay(double amount) {
        System.out.println("微信支付"+ amount + "￥");
    }
}
