package cn.org.xiaosheng;

import org.dromara.easyes.starter.register.EsMapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author XiaoSheng
 * @date 2024/7/27 下午2:33
 */
@SpringBootApplication
@EsMapperScan("cn.org.xiaosheng.mapper.es")
public class EsApplication {
    public static void main(String[] args) {
        SpringApplication.run(EsApplication.class, args);
    }
}

