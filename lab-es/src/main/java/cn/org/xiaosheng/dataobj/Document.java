package cn.org.xiaosheng.dataobj;

import lombok.Data;
import org.dromara.easyes.annotation.IndexName;

/**
 * @author XiaoSheng
 * @date 2024/7/27 下午7:33
 */
@Data
@IndexName
public class Document {

    /**
     * es中的唯一id
     */
    private String id;
    /**
     * 文档标题
     */
    private String title;
    /**
     * 文档内容
     */
    private String content;

}
