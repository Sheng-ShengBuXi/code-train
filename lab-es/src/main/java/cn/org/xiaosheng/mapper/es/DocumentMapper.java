package cn.org.xiaosheng.mapper.es;

import cn.org.xiaosheng.dataobj.Document;
import org.dromara.easyes.core.kernel.BaseEsMapper;

/**
 * @author XiaoSheng
 * @date 2024/7/27 下午7:39
 */
public interface DocumentMapper extends BaseEsMapper<Document> {

}
