package cn.org.xiaosheng.test;

import cn.org.xiaosheng.dataobj.Document;
import cn.org.xiaosheng.mapper.es.DocumentMapper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author XiaoSheng
 * @date 2024/7/27 下午7:40
 */
@SpringBootTest
public class EsTest {

    @Resource
    private DocumentMapper documentMapper;

    @Test
    public void testInsert() {
        // 测试插入数据
        Document document = new Document();
        document.setTitle("老汉");
        document.setContent("推*技术过硬");
        int successCount = documentMapper.insert(document);
        System.out.println(successCount);
    }


}
