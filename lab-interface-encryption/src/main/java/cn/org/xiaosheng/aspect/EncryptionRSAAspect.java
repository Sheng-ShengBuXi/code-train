package cn.org.xiaosheng.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author XiaoSheng
 * @date 2024-05-25
 * @dec 描述
 */
@Aspect
@Component
@Order(2)
@Slf4j
public class EncryptionRSAAspect {

    /**
             * 1> 获取请求参数
             * 2> 获取被请求接口的入参类型
             * 3> 判断是否为get请求 是则跳过AES解密判断
             * 4> 请求参数解密->封装到接口的入参
             */

        @Pointcut("execution(public * cn.org.xiaosheng.controller.*.*(..))")
        public void requestRAS() {

        }





}
