package cn.org.xiaosheng.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 * @author XiaoSheng
 * @date 2024-05-25
 * @dec 描述
 */
@Data
@AllArgsConstructor
@ToString
public class RSADecodeData {

    private String key;

    private String keyVI;

    private long time;
}
