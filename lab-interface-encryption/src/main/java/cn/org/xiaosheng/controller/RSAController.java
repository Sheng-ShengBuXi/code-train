package cn.org.xiaosheng.controller;

import cn.org.xiaosheng.bean.RSADecodeData;
import cn.org.xiaosheng.utils.RSAInitUtil;
import cn.org.xiaosheng.utils.RSAUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.interfaces.RSAPrivateKey;

/**
 * @author XiaoSheng
 * @date 2024-05-27
 * @dec 描述
 */
@RestController
@RequestMapping("/rsa")
public class RSAController {

    @RequestMapping("/getAESEcrypt")
    public String getAESEcrypt(RSADecodeData rsaDecodeData) {
        System.out.println(rsaDecodeData);
        String s = RSAInitUtil.encryptByPrivateKey(rsaDecodeData.getKey() + rsaDecodeData.getKeyVI(), (RSAPrivateKey) RSAUtil.keyPair.getPrivate());
        System.out.println(s);
        return s;
    }



}
