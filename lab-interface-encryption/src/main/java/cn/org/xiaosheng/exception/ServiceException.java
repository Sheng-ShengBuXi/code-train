package cn.org.xiaosheng.exception;

/**
 * @author XiaoSheng
 * @date 2024-05-26
 * @dec 描述
 */
public class ServiceException  extends RuntimeException {

    public ServiceException(String message) {
        super(message);
    }

}
