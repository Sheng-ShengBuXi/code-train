package cn.org.xiaosheng.interfaces;

import java.lang.annotation.*;

/**
 * @author XiaoSheng
 * @date 2024-05-25
 * @dec RSA加密注解
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EncryptionRSA {


}
