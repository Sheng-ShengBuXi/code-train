package cn.org.xiaosheng.utils;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Security;
import java.util.Base64;

/**
 * @author XiaoSheng
 * @date 2024-05-25
 * @dec 描述
 */
public class AES256Util {

    private static final String AES = "AES";
    /**
     * 初始向量IV, 初始向量IV的长度规定为128位16个字节, 初始向量的来源为随机生成.
     */
    /**
     * 加密解密算法/加密模式/填充方式
     */
    private static final String CIPHER_ALGORITHM = "AES/CBC/PKCS7Padding";

    private static final Base64.Encoder base64Encoder = java.util.Base64.getEncoder();
    private static final Base64.Decoder base64Decoder = java.util.Base64.getDecoder();

    /*
     * 解决java不支持AES/CBC/PKCS7Padding模式解密
     */
    static {
        Security.addProvider(new BouncyCastleProvider());
    }

        /**
         * AES加密
         */
        public static String encrypt(String key, String content,String keyVI) {
            try {
                javax.crypto.SecretKey secretKey = new javax.crypto.spec.SecretKeySpec(key.getBytes(), AES);
                javax.crypto.Cipher cipher = javax.crypto.Cipher.getInstance(CIPHER_ALGORITHM);
                cipher.init(javax.crypto.Cipher.ENCRYPT_MODE, secretKey, new javax.crypto.spec.IvParameterSpec(keyVI.getBytes()));
                // 获取加密内容的字节数组(这里要设置为utf-8)不然内容中如果有中文和英文混合中文就会解密为乱码
                byte[] byteEncode = content.getBytes(java.nio.charset.StandardCharsets.UTF_8);
                // 根据密码器的初始化方式加密
                byte[] byteAES = cipher.doFinal(byteEncode);
                // 将加密后的数据转换为字符串
                return base64Encoder.encodeToString(byteAES);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
      }

       /**
         * AES解密
         */
       public static String decrypt(String key, String content,String keyVI) {
           try {
               javax.crypto.SecretKey secretKey = new javax.crypto.spec.SecretKeySpec(key.getBytes(), AES);
               javax.crypto.Cipher cipher = javax.crypto.Cipher.getInstance(CIPHER_ALGORITHM);
               cipher.init(javax.crypto.Cipher.DECRYPT_MODE, secretKey, new javax.crypto.spec.IvParameterSpec(keyVI.getBytes()));
               // 将加密并编码后的内容解码成字节数组
               byte[] byteContent = base64Decoder.decode(content);
               // 解密
               byte[] byteDecode = cipher.doFinal(byteContent);
               return new String(byteDecode, java.nio.charset.StandardCharsets.UTF_8);
           } catch (Exception e) {
               e.printStackTrace();
           }
           return null;
      }





}
