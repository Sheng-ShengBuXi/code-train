package cn.org.xiaosheng.Functions.impl;

import cn.org.xiaosheng.Functions.interfaces.BranchHandle;
import cn.org.xiaosheng.Functions.interfaces.PresentOrElseHandler;
import cn.org.xiaosheng.Functions.interfaces.ThrowExceptionFunction;

public class VUtils {

    /**
     *  如果参数为true抛出异常
     *
     * @param b
     * @return com.example.demo.func.ThrowExceptionFunction
     **/
    public static ThrowExceptionFunction isTure(boolean b){

        return (errorMessage) -> {
            if (b){
                throw new RuntimeException(errorMessage);
            }
        };
    }

    /**
     * 参数为true或false时，分别进行不同的操作
     *
     * @param b
     * @return com.example.demo.func.BranchHandle
     **/
    public static BranchHandle isTureOrFalse(boolean b){

        return (trueHandle, falseHandle) -> {
            if (b){
                trueHandle.run();
            } else {
                falseHandle.run();
            }
        };
    }

    /**
     * 参数为true或false时，分别进行不同的操作
     *
     * @param str
     * @return com.example.demo.func.BranchHandle
     **/
    public static PresentOrElseHandler<?> isBlankOrNoBlank(String str){

        return (consumer, runnable) -> {
            if (str == null || str.length() == 0){
                runnable.run();
            } else {
                consumer.accept(str);
            }
        };
    }


    public static void main(String[] args) {
//        isTure(true).throwMessage("我要抛出异常了!");

        isTureOrFalse(true).trueOrFalseHandle(() ->{
            System.out.println("我是true");
        }, () ->{
            System.out.println("我是false");
        });

        isBlankOrNoBlank("hello")
                .presentOrElseHandle(System.out::println, () -> {
                    System.out.println("空字符串");
                });
        isBlankOrNoBlank("")
                .presentOrElseHandle(System.out::println, () -> {
                    System.out.println("空字符串");
                });

    }

}
