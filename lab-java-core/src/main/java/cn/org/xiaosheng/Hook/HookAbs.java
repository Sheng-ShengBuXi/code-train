package cn.org.xiaosheng.Hook;

/**
 * @author XiaoSheng
 * @date 2024-07-22
 * @dec 描述
 */
public abstract class HookAbs implements HookInterface {

    // 抽象基本方法1
    protected abstract void doSomething();

    // 抽象基本方法2
    protected abstract void doAnything();

    // 抽象基本方法3
    protected abstract void alarm();

    // 接口定义的模板方法
    @Override
    public void templateMethod() {
        this.doSomething();
        if (this.isAlarm()) {
            this.alarm();
        }
        this.doAnything();

    }


    // 钩子方法
    protected boolean isAlarm() {
        return true;
    }

}
