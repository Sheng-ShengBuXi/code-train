package cn.org.xiaosheng.Hook;

/**
 * @author XiaoSheng
 * @date 2024-07-22
 * @dec 描述
 */
public class HookImp extends HookAbs {


    @Override
    protected void doSomething() {
        System.out.println("doSomething执行了");
    }

    @Override
    protected void doAnything() {
        System.out.println("doAnything执行了");
    }

    @Override
    protected void alarm() {
        System.out.println("报警了");
    }

    @Override
    protected boolean isAlarm() {
        return true;
    }
}
