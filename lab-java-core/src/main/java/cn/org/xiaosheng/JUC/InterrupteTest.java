package cn.org.xiaosheng.JUC;

/**
 * @author XiaoSheng
 * @date 2024/8/27 下午2:44
 */
public class InterrupteTest {
    public static void main(String[] args) {
//        Thread timeoutThread = new Thread(() -> {
//            try {
//                Thread.sleep(3000); // 设置超时时间为3秒
//            } catch (InterruptedException e) {
//                Thread.currentThread().interrupt(); // 请求中断
//                // 忽略
//                System.out.println("被中断之后的错误捕获");
//            }
//        });
//        timeoutThread.start();

//        try {
//            // 执行可能阻塞的操作
//            Object obj = new Object();
//            synchronized (obj) {
//                obj.wait(); // 等待
//            }
//        } catch (InterruptedException e) {
//            Thread.currentThread().interrupt(); // 恢复中断状态
//            System.out.println("Operation timed out.");
//        }

        test();
//        Thread.sleep(3000); // 等待一段时间
//        taskThread.interrupt(); // 请求中断
    }

    public static void test() {
        Thread taskThread = new Thread(() -> {
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    System.out.println("Working...");
                    Thread.sleep(5000); // 模拟工作
                }
                System.out.println("Task interrupted.");
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt(); // 恢复中断状态
                System.out.println("Caught InterruptedException.");
            }
        });

        taskThread.start();
        try {
            Thread.sleep(3000); // 等待一段时间
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
//        taskThread.interrupt();
    }
}
