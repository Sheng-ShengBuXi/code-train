package cn.org.xiaosheng.JUC;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author XiaoSheng
 * @date 2024/8/27 下午1:26
 */
public class ProducerConsumerExample {
    private static final SynchronousQueue<String> queue = new SynchronousQueue<>();

    public static void main(String[] args) {
        Thread producer = new Thread(() -> {
            try {
                for (int i = 0; i < 10; i++) {
                    System.out.println("Producing: " + i);
                    // 插入指定的元素。如果当前没有消费者线程等待接收元素，则此方法将一直阻塞，直到有消费者线程取走这个元素。
                    queue.put("item-" + i);
                    TimeUnit.SECONDS.sleep(1);  // 模拟生产时间
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread consumer = new Thread(() -> {
            try {
                for (int i = 0; i < 10; i++) {
                    // 检索并移除队列的头部元素。如果队列为空，则等待队列变为非空。
                    String item = queue.take();
                    System.out.println("Consuming: " + item);
                    TimeUnit.SECONDS.sleep(2);  // 模拟消费时间
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        producer.start();
        consumer.start();
    }
}
