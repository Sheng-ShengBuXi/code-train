package cn.org.xiaosheng.Lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamAnalysis {


    /**
     * 分析执行顺序
     */
    public void executionOrder() {
        List<String> ages = Stream.of(17,22,35,12,37)
                .filter(age -> {
                    System.out.println("filter1 处理：" + age);
                    return age > 18;
                })
                .filter(age -> {
                    System.out.println("filter2 处理：" + age);
                    return age < 35;
                })
                .map(age -> {
                    System.out.println("map 处理：" + age);
                    return age + "岁";
                })
                .collect(Collectors.toList());

        // 等价于如下代码
//        List<Integer> ages = Arrays.asList(17,22,35,12,37);
//        List<String> results = new ArrayList<>();
//        for (Integer age : ages) {
//            if (age > 18) {
//                if (age < 35) {
//                    results.add(age + "岁");
//                }
//            }
//        }
//        System.out.println(results);
    }


    public List<String> sortGetTop3LongWordsByStream(String sentence) {
        return Arrays.stream(sentence.split(" "))
                .filter(word -> word.length() > 5)
                .sorted((o1, o2) -> o2.length() - o1.length())
                .limit(3)
                .collect(Collectors.toList());
    }




    public static void main(String[] args) {
//        new StreamAnalysis().executionOrder();
        String text= "Use LangChain Expression Language, the protocol that LangChain is built on and which facilitates component chaining";
        List<String> strings = new StreamAnalysis().sortGetTop3LongWordsByStream(text);
        System.out.println(strings);

    }

}
