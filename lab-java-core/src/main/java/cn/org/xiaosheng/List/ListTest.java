package cn.org.xiaosheng.List;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author XiaoSheng
 * @date 2024/8/25 下午1:45
 */
public class ListTest {

    public static void main(String[] args) {
        ListTracker listTracker = new ListTracker(new HashMap<>());
        Map<String, SafePoint> locations = listTracker.getLocations();
        SafePoint safePoint = new SafePoint(1, 2);
        locations.put("name", safePoint);
        SafePoint safePoint1 = locations.get("name");
        System.out.println(safePoint1.toString());
        SafePoint name = listTracker.getLocation("name");
        System.out.println(name.toString());
    }
}
