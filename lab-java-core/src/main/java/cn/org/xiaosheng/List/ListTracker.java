package cn.org.xiaosheng.List;

import java.util.Map;

/**
 * @author XiaoSheng
 * @date 2024/8/25 下午1:32
 */
public class ListTracker {

    private final Map<String, SafePoint> locations;

    public ListTracker(Map<String, SafePoint> locations) {
        this.locations = locations;
    }

    public Map<String, SafePoint> getLocations() {
        return locations;
    }

    public SafePoint getLocation(String id) {
        return locations.get(id);
    }

}
