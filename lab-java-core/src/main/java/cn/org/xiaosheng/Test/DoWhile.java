package cn.org.xiaosheng.Test;

/**
 * @author XiaoSheng
 * @date 2024/8/14 下午3:34
 */
public class DoWhile {

    public static void main(String[] args) {
        getString();
    }

    public static String getString() {
        do {
            try {
                int i = 1/0;
                return "2";
            } catch (Exception e) {
                /**
                 * throw与throws关键字区别
                 * throws 使用 throws 声明的方法表示此方法不处理异常，而交给方法的调用处进行处理
                 * throw 关键字的使用完全符合异常的处理机制，但是一般来讲用户都在避免异常的产生，
                 * 所以不会手工抛出一个新的异常类的实例，往往会抛出程序中已经产生的异常类实例。
                 */
                // 抛出异常后循环将不再执行
                e.printStackTrace();
                throw e;
            }

        }while (true);
    }


}
