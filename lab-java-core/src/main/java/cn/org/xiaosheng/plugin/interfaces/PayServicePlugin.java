package cn.org.xiaosheng.plugin.interfaces;

import cn.org.xiaosheng.plugin.context.PayContext;
import org.springframework.plugin.core.Plugin;

public interface PayServicePlugin extends Plugin<PayContext> {

    void pay();

}
