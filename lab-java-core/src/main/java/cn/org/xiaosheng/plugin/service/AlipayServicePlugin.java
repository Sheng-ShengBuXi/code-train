package cn.org.xiaosheng.plugin.service;

import cn.org.xiaosheng.plugin.context.PayContext;
import cn.org.xiaosheng.plugin.interfaces.PayServicePlugin;
import org.springframework.stereotype.Component;

@Component
public class AlipayServicePlugin implements PayServicePlugin {

    @Override
    public void pay() {
        System.out.println("阿里支付");
    }

    @Override
    public boolean supports(PayContext payContext) {
        return payContext.getName().equals("ali");
    }
}
