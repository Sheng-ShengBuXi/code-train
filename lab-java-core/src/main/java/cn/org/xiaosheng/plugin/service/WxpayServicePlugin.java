package cn.org.xiaosheng.plugin.service;

import cn.org.xiaosheng.plugin.context.PayContext;
import cn.org.xiaosheng.plugin.interfaces.PayServicePlugin;
import org.springframework.stereotype.Component;

@Component
public class WxpayServicePlugin implements PayServicePlugin {

    @Override
    public void pay() {
        System.out.println("微信支付");
    }

    @Override
    public boolean supports(PayContext payContext) {
        return payContext.getName().equals("wx");
    }
}
