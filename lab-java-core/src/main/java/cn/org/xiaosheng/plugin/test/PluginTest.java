package cn.org.xiaosheng.plugin.test;

import cn.org.xiaosheng.plugin.context.PayContext;
import cn.org.xiaosheng.plugin.interfaces.PayServicePlugin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.plugin.core.PluginRegistry;
import org.springframework.stereotype.Controller;

import java.util.Optional;

@Controller
public class PluginTest {

    @Autowired
    private PluginRegistry<PayServicePlugin, PayContext> plugins;

    public void test() {
        PayContext payContext = new PayContext();
        payContext.setPrice(101);
        payContext.setName("ali");
        Optional<PayServicePlugin> pluginFor = plugins.getPluginFor(payContext);
        pluginFor.orElseThrow(()-> new IllegalArgumentException("Not Support Pay")).pay();

    }



}

