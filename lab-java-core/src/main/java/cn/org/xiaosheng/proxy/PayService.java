package cn.org.xiaosheng.proxy;

public interface PayService {

    String pay();

}
