package cn.org.xiaosheng.test;

import cn.org.xiaosheng.Hook.HookAbs;
import cn.org.xiaosheng.Hook.HookImp;

/**
 * @author XiaoSheng
 * @date 2024-07-22
 * @dec 描述
 */
public class HookTest {

    public static void main(String[] args) {
        HookAbs hookAbs = new HookImp();
        hookAbs.templateMethod();
    }

}
