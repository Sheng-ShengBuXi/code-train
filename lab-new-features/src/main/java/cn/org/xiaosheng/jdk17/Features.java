package cn.org.xiaosheng.jdk17;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author XiaoSheng
 * @date 2024/8/7 下午2:27
 */
public class Features {
    public static void main(String[] args) {
        System.out.println(getHtmlJDK17());
//        testNPE();
        testRecord();
        testSwitch();
        testInterface();
        testInstanceof("Hello XiaoSheng!");
    }

    /**
     * 使用JDK17返回HTML文本
     *
     * @return 返回HTML文本
     */
    public static final String getHtmlJDK17() {
        return """
        <html>
            <body>
                <p>Hello, world</p>
            </body>
        </html>
        """;
    }

    /**
     * NullPointerException增强
     * NPE原因代码提示到行
     */
    public static void testNPE() {
        try {
            //简单的空指针
            String str = null;
            str.length();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            //复杂一点的空指针
            var arr = List.of(null);
            String str = (String)arr.get(0);
            str.length();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Records类
     *
     */
    public static void testRecord() {
        StudentRecords record = new StudentRecords(1L, "张三", 16, "男", "xxx@qq.com");
        System.out.println(record);
        System.out.println(record.stuName());
    }


    /**
     * Switch表达式
     */
    public static void testSwitch() {
        SwitchDemo switchDemo = new SwitchDemo();
        System.out.println(switchDemo.getByJDK17(SwitchDemo.Week.MONDAY));
    }

    /**
     * Interface的私有接口方法
     * 从Java8开始，允许在interface里面添加默认方法，其实当时就有些小困惑，如果一个default方法体很大怎么办，拆到另外的类去写吗？实在有些不太合理，所以在Java17里面，如果一个default方法体很大，那么可以通过新增接口私有方法来进行一个合理的拆分了，为这个小改进点个赞。
     *
     */
    public static  void testInterface() {
        InterfaceMethod interfaceMethod = new InterfaceMethod();
        interfaceMethod.defaultMethod();
    }

    /**
     * 模式匹配
     * 在JDK 17中，模式匹配主要用于instanceof表达式。模式匹配增强了instanceof的语法和功能，使类型检查和类型转换更加简洁和高效。在传统的Java版本中，我们通常使用instanceof结合类型转换来判断对象类型并进行处理，这往往会导致冗长的代码。
     *
     */
    public static void testInstanceof(Object value) {
        if (value instanceof String) {
            String v = (String)value;
            System.out.println("遇到一个String类型" + v.toUpperCase());
        } else if (value instanceof Integer) {
            Integer v = (Integer)value;
            System.out.println("遇到一个整型类型" + v.longValue());
        }

        /**
         * 新写法
         * 转换并申请了一个新的变量，极大地方便了代码的编写
         *
         */
        if (value instanceof String v) {
            System.out.println("遇到一个String类型" + v.toUpperCase());
        } else if (value instanceof Integer v) {
            System.out.println("遇到一个整型类型" + v.longValue());
        }
    }

    /**
     * 集合类的工厂方法
     *
     */
    public static void testList() {
        Set<String> set = new HashSet<>();
        set.add("a");
        set.add("b");
        set.add("c");

        Set<String> set1 = Set.of("a", "b", "c");

    }


}
