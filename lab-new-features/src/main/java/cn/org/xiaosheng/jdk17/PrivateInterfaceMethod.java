package cn.org.xiaosheng.jdk17;

/**
 * @author XiaoSheng
 * @date 2024/8/7 下午3:05
 */
public interface PrivateInterfaceMethod {
        /**
         * 接口默认方法
         */
        default void defaultMethod() {
            privateMethod();
        }

        // 接口私有方法，在Java8里面是不被允许的，不信你试试
        private void privateMethod() {
            System.out.println("Hello XiaoSheng!");
        }
}

