package cn.org.xiaosheng.jdk17;

/**
 * @author XiaoSheng
 * @date 2024/8/7 下午2:35
 */
public record StudentRecords(Long stuId,
                             String stuName,
                             int stuAge,
                             String stuGender,
                             String stuEmail) {

    public StudentRecords {
        System.out.println("构造函数");
    }

    @Override
    public String toString() {
        return "";
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        return false;
    }
}
