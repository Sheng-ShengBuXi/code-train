package cn.org.xiaosheng.jdk17;

/**
 * @author XiaoSheng
 * @date 2024/8/7 下午2:50
 */

public class SwitchDemo {
    /**
     * 在JDK8中获取switch返回值方式
     *
     * @param week
     * @return
     */
    public int getByJDK8(Week week) {
        int i = 0;
        switch (week) {
            case MONDAY, TUESDAY:
                i = 1;
                break;
            case WEDNESDAY:
                i = 3;
                break;
            case THURSDAY:
                i = 4;
                break;
            case FRIDAY:
                i = 5;
                break;
            case SATURDAY:
                i = 6;
                break;
            case SUNDAY:
                i = 7;
                break;
            default:
                i = 0;
                break;
        }

        return i;
    }

    /**
     * 在JDK17中获取switch返回值
     *
     * @param week
     * @return
     */
    public int getByJDK17(Week week) {
        // 1, 现在的switch变成了表达式，可以返回值了，而且支持yield和->符号来返回值
        // 2, 再也不用担心漏写了break，而导致出问题了
        // 3, case后面支持写多个条件
        return switch (week) {
            case MONDAY -> 1;
            case TUESDAY -> 2;
            case WEDNESDAY -> 3;
            case THURSDAY -> {yield 4;}
            case FRIDAY -> 5;
            case SATURDAY, SUNDAY -> 6;
            default -> 0;
        };
    }

    public enum Week {
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY,
        SUNDAY
    }
}

