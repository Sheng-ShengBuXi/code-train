package cn.org.xiaosheng;

import cn.org.xiaosheng.applicationContext.TestApplicationContextInitializer;
import cn.org.xiaosheng.beanFactory.TestFactoryBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Collections;

/**
 * @author XiaoSheng
 * @date 2024/8/18 下午2:31
 */
@SpringBootApplication
public class Main {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication();
        springApplication.addInitializers(new TestApplicationContextInitializer());
        springApplication.addPrimarySources(Collections.singleton(Main.class));
        ConfigurableApplicationContext run = springApplication.run(args);
    }
}