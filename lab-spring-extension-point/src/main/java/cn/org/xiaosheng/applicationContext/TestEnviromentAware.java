package cn.org.xiaosheng.applicationContext;

import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * @author XiaoSheng
 * @date 2024/8/18 下午8:39
 */
@Configuration
public class TestEnviromentAware implements EnvironmentAware {

    @Override
    public void setEnvironment(Environment environment) {
        System.out.println("[TestEnviromentAware]" +environment);
//        setEnvironment(environment);
    }
}
