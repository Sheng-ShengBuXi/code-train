package cn.org.xiaosheng.beanDefinition;

import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * @author XiaoSheng
 * @date 2024/8/18 下午8:52
 */
@Configuration
public class NormalBeanA implements BeanNameAware, InitializingBean {

    public NormalBeanA() {
        System.out.println("NormalBean constructor");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("[InitializingBean] NormalBeanA");
    }

    @Override
    public void setBeanName(String s) {
        if ("normalBeanA".equals(s)) {
            s = "xiaoshengBeanA";
        }
        System.out.println("[BeanNameAware] " + s);
    }

    @PostConstruct
    public void init()  {
        System.out.println("[PostConstruct] NormalBeanA");
    }
}
