package cn.org.xiaosheng.beanFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

/**
 * @author XiaoSheng
 * @date 2024/8/18 下午9:31
 */
@Configuration
public class TestCommandLineRunner implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        System.out.println("[TestCommandLineRunner]");
    }
}
