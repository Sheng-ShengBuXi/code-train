package cn.org.xiaosheng.beanFactory;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Configuration;

/**
 * @author XiaoSheng
 * @date 2024/8/18 下午9:15
 */
@Configuration
public class TestFactoryBean implements FactoryBean<TestFactoryBean.TestFactoryInnerBean> {


    @Override
    public TestFactoryInnerBean getObject() throws Exception {
        System.out.println("[FactoryBean] getObject");
        return new TestFactoryBean.TestFactoryInnerBean();
    }

    @Override
    public Class<?> getObjectType() {
        return TestFactoryBean.TestFactoryInnerBean.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    public static class TestFactoryInnerBean{

    }
}
