package lab.springai.alibaba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LabSpringaiAlibabaApplication {

    public static void main(String[] args) {
        SpringApplication.run(LabSpringaiAlibabaApplication.class, args);
    }

}
