package cn.org.xiaosheng.service;

/**
 * @author XiaoSheng
 * @date 2024-04-24
 * @dec 描述
 */
public class MyService {

    public Integer getThreeData() {
        // 模拟调用第三方接口耗时
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 1;
    }

    public Integer doServic() {
        // 模拟调用第三方接口耗时
        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 3;
    }

    public Integer doServic2() {
        // 模拟调用第三方接口耗时
        try {
            Thread.sleep(2000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 5;
    }


}
