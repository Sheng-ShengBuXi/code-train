package cn.org.xiaosheng.test;

import cn.org.xiaosheng.service.MyService;
import cn.org.xiaosheng.threadPool.MyThreadPoolExecutor;

import javax.swing.*;
import java.util.Comparator;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Stream;

/**
 * @author XiaoSheng
 * @date 2024-04-24
 * @dec 多线程测试
 */
public class MyThreadTest {

    /**
     * 通过多线程进行任务分解
     */
    public void threadPoolTest1() {
        System.out.println("多线程执行任务开始前: " + System.currentTimeMillis());
        ThreadPoolExecutor threadPool = new MyThreadPoolExecutor().getThreadPool();
        MyService myService = new MyService();
        Future<Integer> submit = threadPool.submit(() -> {
            return myService.getThreeData();
        });

        Future<Integer> submit1 = threadPool.submit(() -> {
            return myService.doServic2();
        });

        Future<Integer> submit2 = threadPool.submit(() -> {
            return myService.doServic();
        });
        Integer integer = Stream.of(submit, submit1, submit2).map(intCode -> {
            try {
                return intCode.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return null;
        }).filter(Objects::nonNull).min(Integer::compareTo).get();
        // 注意: Future需要调用get进行线程阻塞获取值, 各线程才真正进行执行, 否则,按照Future机制不会执行相关业务操作。
        System.out.println("多线程执行任务结束后: " + System.currentTimeMillis() + "返回值为: " + 1);
    }

    /**
     * 直接执行各任务(串行方式), 不采用异步方式
     */
    public void directCall() {
        MyService myService = new MyService();
        System.out.println("直接执行任务前: " + System.currentTimeMillis());
        Integer threeData = myService.getThreeData();
        Integer integer = myService.doServic();
        Integer integer1 = myService.doServic2();
        Integer integer2 = Stream.of(threeData, integer, integer).filter(Objects::nonNull).min(Integer::compareTo).get();
        System.out.println("直接执行完成后: " + System.currentTimeMillis() + "返回值为:" + integer2);

    }


    /**
     *  CompletableFuture方式
     *  https://juejin.cn/post/7124124854747398175
     *  https://blog.csdn.net/weixin_45723088/article/details/123579939
     */
    public Integer completableFuture() {
        System.out.println("使用CompletableFuture方法前: " + System.currentTimeMillis());
        MyService myService = new MyService();
        CompletableFuture<Integer> integerCompletableFuture = CompletableFuture.supplyAsync(() -> myService.doServic());
        CompletableFuture<Integer> integerCompletableFuture1 = CompletableFuture.supplyAsync(() -> myService.doServic2());
        CompletableFuture<Integer> integerCompletableFuture2 = CompletableFuture.supplyAsync(() -> myService.getThreeData());
        Integer integer =  Stream.of(integerCompletableFuture, integerCompletableFuture1, integerCompletableFuture2)
                .map(CompletableFuture::join).sorted(Integer::compareTo).findFirst().get();
        System.out.println("使用CompletableFuture方法后: " + System.currentTimeMillis() + "返回值为: " + integer);
        return integer;
    }

    /**
     * 测试异常
     */
    public void completableFutureExection() {
        CompletableFuture.supplyAsync(() -> {
            throw new RuntimeException("supplyAsync excetion occurred...");
        }).handle((obj, e) -> {
            if (e != null) {
                System.out.println("thenApply executed, exception occurred...");
            }
            return obj;
        }).join();
    }

    public static void main(String[] args) {
        MyThreadTest myThreadTest = new MyThreadTest();
        //myThreadTest.directCall();  // 耗时大概: 6s
        //myThreadTest.threadPoolTest1(); // 耗时大概: 3s 取决于耗时最大的查询操作了
        //myThreadTest.completableFuture();
        myThreadTest.completableFutureExection();
        Thread thread = new Thread();

    }



}
