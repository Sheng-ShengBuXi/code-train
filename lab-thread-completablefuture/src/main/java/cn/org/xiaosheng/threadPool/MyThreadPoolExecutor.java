package cn.org.xiaosheng.threadPool;

import java.util.concurrent.*;

/**
 * @author XiaoSheng
 * @date 2024-04-24
 * @dec 自定义线程池
 */
public class MyThreadPoolExecutor {


    /**
     * 创建线程池的几种方式
     */
    /**
     * 使用 Executors 工厂方法创建：
     *
     * Executors 类提供了一些方便的工厂方法来创建不同类型的线程池。
     */
    //ExecutorService threadPool = Executors.newFixedThreadPool(int nThreads);
    //ExecutorService threadPool = Executors.newSingleThreadExecutor();
    //ExecutorService threadPool = Executors.newCachedThreadPool();
    //ExecutorService threadPool = Executors.newScheduledThreadPool(int corePoolSize);

    /**
     * 使用 ThreadPoolExecutor 构造函数创建：
     *
     * ThreadPoolExecutor 类本身也提供了多个构造函数来创建线程池。
     */
//    ThreadPoolExecutor threadPool = new ThreadPoolExecutor(
//    int corePoolSize, // 核心线程数
//    int maximumPoolSize, // 最大线程数
//    long keepAliveTime,时间单位
//    BlockingQueue<Runnable> workQueue, // 任务队列
//    ThreadFactory threadFactory, // 线程工厂
//    RejectedExecutionHandler handler // 拒绝策略
//);

    ThreadPoolExecutor threadPool = new ThreadPoolExecutor(
            5, // 核心线程数
            10, // 最大线程数
            1, // 空闲线程存活时间为1秒
            TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(100), // 任务队列大小为100
            Executors.defaultThreadFactory(), // 使用默认线程工厂
            new ThreadPoolExecutor.AbortPolicy() // 拒绝策略为中止策略
    );

    public ThreadPoolExecutor getThreadPool() {
        return threadPool;
    }

}
