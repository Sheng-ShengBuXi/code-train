package cn.org.xiaosheng.threadlocal;

public class InheritableThreadTest {

    public static void main(String[] args) {
        ThreadLocal<String> threadLocal = new ThreadLocal<>();
        InheritableThreadLocal<String> inheritableThreadLocal = new InheritableThreadLocal<>();

        threadLocal.set("你好 ");
        inheritableThreadLocal.set("xiaosheng");

        // 子线程中获取不到threadLocal变量, 线程隔离  可以获取inheritableThreadLocal变量
        Thread thread = new Thread(() -> {
            System.out.println(threadLocal.get());
            System.out.println(inheritableThreadLocal.get());
        });
        thread.start();
    }

}
