package cn.org.xiaosheng.threadlocal;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class ThreadLocalTest1 {

    public static void main(String[] args) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                ThreadLocal<XSDTO> tl = new ThreadLocal<>();
                tl.set(new XSDTO("XiaoSheng"));
                System.out.println(tl.get());
                ThreadLocal<XSDTO> tl1 = new ThreadLocal<>();
                tl1.set(new XSDTO("Hello "));
            }
        });
        t.start();

        Object object = new Object();
        WeakReference<Object> testWeakReference = new WeakReference<>(object);
        System.out.println("GC回收之前，弱引用："+testWeakReference.get());
        //触发系统垃圾回收
        System.gc();
        System.out.println("GC回收之后，弱引用："+testWeakReference.get());
        //手动设置为object对象为null
        object=null;
        System.gc();
        System.out.println("对象object设置为null，GC回收之后，弱引用："+testWeakReference.get());

        List<Object> list = new ArrayList<>();
        Object object1 = new Object();
        list.add(object1);
        object1 = null;
        System.out.println(list.size());
        System.out.println(list.get(0));

    }

}
