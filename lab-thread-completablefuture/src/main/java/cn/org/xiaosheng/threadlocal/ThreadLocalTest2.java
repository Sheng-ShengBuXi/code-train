package cn.org.xiaosheng.threadlocal;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadLocalTest2 {

    private static ThreadLocal<XSDTO> tianLuoThreadLocal = new ThreadLocal<>();

    public static void main(String[] args) throws InterruptedException {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5, 5, 1, TimeUnit.MINUTES, new LinkedBlockingQueue<>());

        for (int i = 0; i < 10; ++i) {
            threadPoolExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    System.out.println("创建对象：");
                    XSDTO xsdto = new XSDTO("xiaosheng");
                    tianLuoThreadLocal.set(xsdto);
                    xsdto = null; //将对象设置为 null，表示此对象不在使用了
                    // tianLuoThreadLocal.remove();

                }
            });
            Thread.sleep(1000);
        }


    }
}
