package cn.org.xiaosheng.threadlocal;

public class XSDTO {

    private byte[] bytes = new byte[100 * 1024 * 1024];

    private String name;

    public XSDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "XSDTO{" +
                "name='" + name + '\'' +
                '}';
    }
}
