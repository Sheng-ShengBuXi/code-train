package cn.org.xiaosheng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.web.reactive.config.EnableWebFlux;

/**
 * @author XiaoSheng
 * @date 2024-05-20
 * @dec 描述
 */
// 排除SpringMVC
@SpringBootApplication(exclude = WebMvcAutoConfiguration.class)
@EnableWebFlux
public class ChatMsgApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChatMsgApplication.class, args);
    }

}
