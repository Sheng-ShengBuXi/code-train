package cn.org.xiaosheng.bean;

import java.time.LocalDateTime;

/**
 * @author XiaoSheng
 * @date 2024/9/9 下午11:25
 */
public class Logs {

    // 主键
    private Long id;

    // 访问者IP地址
    private String ipAddr;

    // 访问时间
    private LocalDateTime accessTime;

    // 详细请求简写
    private String simpleRequest;

    // 请求状态编码
    private String requestNum;

    // 请求体body字节长度
    private Long bodyLength;

    // http-user-agent
    private String httpAgent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIpAddr() {
        return ipAddr;
    }

    public void setIpAddr(String ipAddr) {
        this.ipAddr = ipAddr;
    }

    public LocalDateTime getAccessTime() {
        return accessTime;
    }

    public void setAccessTime(LocalDateTime accessTime) {
        this.accessTime = accessTime;
    }

    public String getSimpleRequest() {
        return simpleRequest;
    }

    public void setSimpleRequest(String simpleRequest) {
        this.simpleRequest = simpleRequest;
    }

    public String getRequestNum() {
        return requestNum;
    }

    public void setRequestNum(String requestNum) {
        this.requestNum = requestNum;
    }

    public Long getBodyLength() {
        return bodyLength;
    }

    public void setBodyLength(Long bodyLength) {
        this.bodyLength = bodyLength;
    }

    public String getHttpAgent() {
        return httpAgent;
    }

    public void setHttpAgent(String httpAgent) {
        this.httpAgent = httpAgent;
    }

    @Override
    public String toString() {
        return "Logs{" +
                "id=" + id +
                ", ipAddr='" + ipAddr + '\'' +
                ", acessTime=" + accessTime +
                ", simpleRequest='" + simpleRequest + '\'' +
                ", requestNum='" + requestNum + '\'' +
                ", bodyLength=" + bodyLength +
                ", httpAgent='" + httpAgent + '\'' +
                '}';
    }
}
