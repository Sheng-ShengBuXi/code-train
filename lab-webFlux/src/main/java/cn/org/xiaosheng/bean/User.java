package cn.org.xiaosheng.bean;

/**
 * @author XiaoSheng
 * @date 2024/9/10 下午12:47
 */
public class User {
    /**
     * 姓名
     */
    private String name;
    /**
     * 描述
     */
    private String desc;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
