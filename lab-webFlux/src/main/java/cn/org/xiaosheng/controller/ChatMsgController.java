package cn.org.xiaosheng.controller;

import cn.org.xiaosheng.config.ChatMsgFluxUnit;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * @author XiaoSheng
 * @date 2024-05-20
 * @dec 描述
 */
@RestController
@RequestMapping("/chatMsg")
public class ChatMsgController {

    @Resource
    private ChatMsgFluxUnit<String, Map<String,String>> chatMsgFluxUnit;

    @GetMapping(value = "/flux",produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    @CrossOrigin(origins = "*")
    public Flux<Map<String, String>> flux(@RequestParam("content") String content){
        System.out.println(content);
        return chatMsgFluxUnit.getMoreChatMsg("你要问啥？请详细描述一下你的问题......,wqwqwqwqwqwwwwwwwwwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqwqsasasasawqwqw", s -> {
            Map<String, String> map = new HashMap<>(1);
            map.put("msg", s);
            return map;
        });
    }




}
