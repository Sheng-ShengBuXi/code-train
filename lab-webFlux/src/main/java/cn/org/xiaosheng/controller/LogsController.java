package cn.org.xiaosheng.controller;

import cn.org.xiaosheng.bean.User;
import cn.org.xiaosheng.service.AnalysisLogsService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author XiaoSheng
 * @date 2024/9/10 上午2:27
 */
@RestController
@RequestMapping("/logs")
public class LogsController {

    @Resource
    private AnalysisLogsService analysisLogsService;

    @GetMapping("/renewLogs")
    @CrossOrigin(origins = "*")
    public void analysisLogs() {
        analysisLogsService.renewLogs();
    }

    // 初步认为webflux只能推送流类型数据,而不能推送json类型数据
    @GetMapping(value = "/logsDisplay", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    @CrossOrigin(origins = "*")
    public Flux<Map<String, Object>> logsDisplay() {
        // 需要注意返回的应该是流式的数据,而不是静态的对象
        return analysisLogsService.logsDisplay().onErrorResume(e -> Flux.empty());
    }

    @GetMapping("/delLogs")
    public void delLogs() {
        analysisLogsService.delLogs();
    }
    
}
