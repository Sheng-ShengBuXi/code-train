package cn.org.xiaosheng.mapper;

import cn.org.xiaosheng.bean.Logs;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author XiaoSheng
 * @date 2024/9/10 上午2:37
 */
@Mapper
public interface LogsMapper {

    Integer insertBatch(List<Logs> logs);

    List<Map<String, Object>> logsDisplay();

    void delLogs();
}
