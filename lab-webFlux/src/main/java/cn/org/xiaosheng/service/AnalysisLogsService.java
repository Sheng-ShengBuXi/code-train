package cn.org.xiaosheng.service;

import cn.org.xiaosheng.bean.Logs;
import cn.org.xiaosheng.mapper.LogsMapper;
import cn.org.xiaosheng.utils.LogExtractor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import javax.annotation.Resource;
import java.io.*;
import java.util.*;

/**
 * 解析日志的服务端
 * @author XiaoSheng
 * @date 2024/9/9 下午11:17
 */
@Service
public class AnalysisLogsService {

    @Resource
    private LogsMapper logsMapper;

    // 日志读取路径
    private final String LOG_PATH = "/usr/local/nginx/logs/access.log";

    public boolean renewLogs() {
        // 1. 解析日志
        List<Logs> maps = analysisLogs();
        // 2. 分批次新增日志到数据库
        int batchSize = 30;
        List<Logs> batch = new LinkedList<>();
        for (Logs log : maps) {
            batch.add(log);
            if (batch.size() == batchSize) {
                logsMapper.insertBatch(batch);
                batch.clear(); // 清空当前批次
            }
        }
        if (!batch.isEmpty()) {
            // 插入剩余的数据
            logsMapper.insertBatch(batch);
        }
        // 2. 分批次新增日志到数据库
//        int batchSize = 50; // 每次插入50条记录
//        for (int i = 0; i < logs.size(); i += batchSize) {
//            int end = Math.min(i + batchSize, logs.size());
//            List<Logs> batch = logs.subList(i, end);
//            logsMapper.insertBatch(batch);
//        }
        // 3.清空文件
        cleanFile(LOG_PATH);
        // 4. 返回处理结果
        return true;
    }


    public Flux<Map<String, Object>> logsDisplay() {
        List<Map<String, Object>> maps = logsMapper.logsDisplay();
        return Flux.fromIterable(maps);
    }

    public void delLogs() {
        logsMapper.delLogs();
    }


    // 解析日志文件
    private List<Logs> analysisLogs() {
        LinkedList<Logs> list = new LinkedList<Logs>();
        try (BufferedReader reader = new BufferedReader(new FileReader(LOG_PATH))) {
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
                Logs entry = LogExtractor.extractLogInfo(line);
                if (entry.getIpAddr() != null) {
                    list.add(entry);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return list;
    }

    // 清空文件
    private void cleanFile(String filePath) {
        try (PrintWriter writer = new PrintWriter(new FileWriter(filePath))) {
            // 不写任何东西，只是打开文件并关闭，这样就清空了文件
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
