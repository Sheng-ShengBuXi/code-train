package cn.org.xiaosheng.tasks;

import cn.org.xiaosheng.mapper.LogsMapper;
import cn.org.xiaosheng.service.AnalysisLogsService;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDateTime;

/**
 * @author XiaoSheng
 * @date 2024/9/10 下午9:54
 */
@Component
@EnableScheduling
public class ScheduledTasks {

    @Resource
    private AnalysisLogsService service;

    // 定义一个每5秒执行一次的任务
//    @Scheduled(fixedDelay = 5000)
//    public void reportCurrentTime() {
//        System.out.println("The time is now " + LocalDateTime.now());
//    }

    // 每天凌晨1点执行的任务
    @Scheduled(cron = "0 1 * * * ?")
    public void runAtNight() {
        service.renewLogs();
    }
}
