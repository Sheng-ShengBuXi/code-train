package cn.org.xiaosheng.utils;


import cn.org.xiaosheng.bean.Logs;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.SignStyle;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * @author XiaoSheng
 * @date 2024/9/9 下午11:58
 */
public class LogExtractor {

    public static void main(String[] args) {
        String logLine = "27.18.107.54|-|[09/Sep/2024:23:10:12 +0800]|\"GET /interview/ HTTP/1.1\"|304|0|\"-\"|\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36\"|\"-\"";
//        String logLine = "27.18.107.54|-|[09/01/2024:23:10:12 +0800]|\"GET /interview/ HTTP/1.1\"|304|0|\"-\"|\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36\"|\"-\"";

        extractLogInfo(logLine);
    }

    public static HashMap<String, String> monthMap = new HashMap<>();

    static {
        monthMap.put("jan", "01");
        monthMap.put("feb", "02");
        monthMap.put("mar", "03");
        monthMap.put("apr", "04");
        monthMap.put("may", "05");
        monthMap.put("jun", "06");
        monthMap.put("jul", "07");
        monthMap.put("aug", "08");
        monthMap.put("sep", "09");
        monthMap.put("oct", "10");
        monthMap.put("nov", "11");
        monthMap.put("dec", "12");
    }

    public static Logs extractLogInfo(String logLine) {


        // 定义一个正则表达式模式来匹配各个字段
        Pattern pattern = Pattern.compile("([^|]+)\\|");
        // 对月份进行处理

        Matcher matcher = pattern.matcher(logLine);
        int count = 0;
        // 实例化需要保存的日志对象
        Logs logs = new Logs();
        while (matcher.find()) {
            // 提取每个字段
            String field = matcher.group(1).trim();
            switch (count) {
                case 0:
                    System.out.println("IP Address: " + field);
                    logs.setIpAddr(field);
                    break;
                case 1:
                    System.out.println("User: " + field);
                    break;
                case 2:
                    // 使用正则表达式提取时间戳
                    String timestamp = logLine.split("\\|")[2].replaceAll("\\[", "").replaceAll("\\]", "").trim();
                    String[] parts = timestamp.split("/");
                    String month = parts[1];
                    // 替换月份缩写为数字
                    String monthNumber = monthMap.get(month.toLowerCase());
                    // 构建新的时间戳字符串
                    String newTimestamp = parts[0] + "/" + monthNumber + "/" + parts[2];
                    LocalDateTime localDateTime = convertToLocalDateTime(newTimestamp);
                    logs.setAccessTime(localDateTime);
                    break;
                case 3:
//                    System.out.println("Request: " + field.replaceAll("\"", ""));
                    logs.setSimpleRequest(field.replaceAll("\"", ""));
                    break;
                case 4:
                    System.out.println("Status: " + field);
                    logs.setRequestNum(field);
                    break;
                case 5:
                    System.out.println("Body Bytes Sent: " + field);
                    if (field.equals("'") || field.equals("") || null == field) {
                        logs.setBodyLength(0L);
                    } else {
                        logs.setBodyLength(Long.parseLong(field));
                    }
                    break;
                case 6:
                    System.out.println("Referer: " + field.replaceAll("\"", ""));
                    break;
                case 7:
                    System.out.println("User Agent: " + field.replaceAll("\"", ""));
                    logs.setHttpAgent(field.replaceAll("\"", ""));
                    break;
                case 8:
                    System.out.println("Additional Info: " + field.replaceAll("\"", ""));
                    break;
                default:
                    break;
            }
            count++;
            System.out.println(logs);

        }
        return logs;
    }

    public static LocalDateTime convertToLocalDateTime(String timestamp) {
        // 定义日期格式
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy:HH:mm:ss Z");

        // 解析为 ZonedDateTime
        ZonedDateTime zonedDateTime = ZonedDateTime.parse(timestamp, formatter);

        // 转换为 LocalDateTime
        LocalDateTime localDateTime = zonedDateTime.toLocalDateTime();

        System.out.println("ZonedDateTime: " + zonedDateTime);
        System.out.println("LocalDateTime: " + localDateTime);
        // 解析日期时间字符串
        return localDateTime;
    }
}
